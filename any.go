// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"hash"
	"io"
)

type AnyMarshaler interface {
	VampAnyType() (Type, any)
}

var Any anyType

type anyType struct{}

func (anyType) FixSize() uint { return typeIO.FixSize() }

func (anyType) DynNo() uint { return typeIO.DynNo() + 1 }

func (t anyType) Write(b *Buffer, value any) error {
	if vm, ok := value.(AnyMarshaler); ok {
		vt, v := vm.VampAnyType()
		return t.WriteAny(b, vt, v)
	}
	return cantWrite(value, "any")
}

func (anyType) WriteAny(b *Buffer, t Type, v any) error {
	err := WriteType(b, t)
	if err != nil {
		return err
	}
	rev := b.sliceFix(sizeOf(&typeIO, b.Offset()))
	defer b.Unslice(rev)
	if err = b.writeOff(0); err != nil {
		return err
	}
	b.appendSlice(sizeOf(t, b.Offset()))
	return t.Write(b, v)
}

func (anyType) ReadAny(b *ReadBuffer) (t Type, v any, err error) {
	t, err = ReadType(b)
	if err != nil {
		return nil, nil, err
	}
	tioSz := sizeOf(&typeIO, b.Offset())
	off := b.readOff(tioSz)
	rev := b.sliceFix(tioSz + b.Offset().Size() + off)
	err = t.Read(b, &v)
	b.Unslice(rev)
	if err != nil {
		return t, nil, err
	}
	return t, v, nil
}

func (t anyType) Read(b *ReadBuffer, into any) (err error) {
	if into, ok := into.(*any); ok {
		_, *into, err = t.ReadAny(b)
		return err
	}
	return cantRead("any", into)
}

func (anyType) Compare(t Type, _ CompareOpts) (bool, int) { return sameAs[anyType](t) }

func (anyType) Hash(h hash.Hash, _ CompareOpts) { h.Write([]byte{anyAlt}) }

func (anyType) String() string { return "any" }

func (t anyType) PrintTo(w io.Writer) (int, error) {
	return io.WriteString(w, t.String())
}

func (anyType) VampUnionAlt() (uint, any) { return anyAlt, &Any }
