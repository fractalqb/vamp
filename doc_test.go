// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"fmt"
	"strings"
)

func Example() {
	type Animal struct {
		Age    int      `vamp:"age,size=8"`
		Name   string   `vamp:",size=8"`
		Owners []string `vamp:",size=8:8"`
		Male   bool
	}
	// Use reflection to get the Vampire type; can be created with API, too
	animalType, _, _ := ReflectType(Animal{})
	// We use a 16-bit offset for the buffers
	const BufferOffset = Size16
	// Create write buffer starting with a Vampire header
	wbuf := NewWriteBuffer(
		animalType,
		BufferOffset,
		HeaderSize,                       // Start writing data behind the header
		HeaderFor(BufferOffset).Bytes(0), // Pass Vampire header as buffer
	)
	// Write a record of type t to the buffer. Value has to match type t.
	animalType.Write(wbuf, Animal{
		Age:    4,
		Name:   "Lassie",
		Owners: []string{"Timmy", "Martin"},
		Male:   false,
	})
	// How many bytes did we write
	fmt.Printf("With header: %d; Value data only: %d\n", len(wbuf.AllBytes()), len(wbuf.Bytes()))
	// Now extract the complete buffer, incl. header
	data := wbuf.AllBytes()
	// Wrap data into a read buffer that expects the header at byte 0
	// Reads buffer's offset size from header (We could also directly read from wbuf)
	rbuf, _ := NewReadBufferHeader(animalType, data, false)
	// We need the specific type – record – to select a field
	rt := animalType.(*Record)
	// Slice the read buffer to field "Owners" with index 2 (3rd field)
	revert := rt.Field(rbuf, 2)
	// Read the Owners into a string array
	var owners []string
	rt.FieldType(2).Read(rbuf, &owners)
	// We want to unslice the buffer to return to the complete record
	rbuf.Unslice(revert)
	// Print what we got from the binary data
	fmt.Println(strings.Join(owners, " & "))
	// Output:
	// With header: 35; Value data only: 31
	// Timmy & Martin
}
