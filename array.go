// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"fmt"
	"hash"
	"io"
	"reflect"
	"strings"

	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/vamp/internal"
)

type ArrayMarshaler interface {
	VampArrayLen() uint
	VampArrayWriter(i uint) (any, error)
}

type ArrayUnmarshaler interface {
	VampArrayResize(l uint) error
	VampArrayReader(i uint) (any, error)
	VampArrayFinish() error
}

type Array struct {
	lenRange SizeRange
	elmType  Type
}

func NewArray(length SizeRange, elem Type) *Array {
	return &Array{
		lenRange: length,
		elmType:  Anonymized(elem),
	}
}

// NewOptional returns an ArrayType with length of Size8 – the smallest
// sufficient size range. This is the recommended way to represent optional
// values.
func NewOptional(elem Type) *Array { return NewArray(Size8, elem) }

func (t *Array) LenRange() SizeRange { return t.lenRange }

func (t *Array) Elem() Type { return t.elmType }

func (t *Array) FixSize() uint { return t.lenRange.Size() }

func (t *Array) DynNo() uint { return 1 }

func (t *Array) Write(buf *Buffer, ls any) (err error) {
	if m, ok := ls.(ArrayMarshaler); ok {
		return t.WriteArray(buf, m)
	}
	lsVal := reflect.ValueOf(ls)
	switch lsVal.Kind() {
	case reflect.Slice, reflect.Array:
		return t.WriteArray(buf, arrayValWrite(lsVal))
	case reflect.Map:
		return t.writeMap(buf, lsVal)
	}
	return cantWrite(ls, "array")
}

func (t *Array) WriteArray(buf *Buffer, ls ArrayMarshaler) error {
	no := ls.VampArrayLen()
	revert, esz, err := t.writePrepare(buf, no)
	switch {
	case err != nil:
		return err
	case no == 0:
		return nil
	}
	defer buf.Unslice(revert)
	for i := uint(0); i < no; i++ {
		e, err := ls.VampArrayWriter(i)
		if err != nil {
			return errAt(t, i, err)
		}
		if err = t.elmType.Write(buf, e); err != nil {
			return errAt(t, i, err)
		}
		buf.sliceFix(esz)
	}
	return nil
}

func (t *Array) WriteMap(buf *Buffer, m any) error {
	mv := reflect.ValueOf(m)
	if mv.Kind() != reflect.Map {
		return fmt.Errorf("cannot write %T to array as map", m)
	}
	return t.writeMap(buf, mv)
}

func (t *Array) WriteOptNone(buf *Buffer) error {
	return t.WriteArray(buf, arrayEmpty{})
}

func (t *Array) WriteOptOne(buf *Buffer, value any) error {
	return t.WriteArray(buf, arrayOptVal{value})
}

func (t *Array) Read(buf *ReadBuffer, into any) error {
	switch out := into.(type) {
	case ArrayUnmarshaler:
		return t.ReadArray(buf, out)
	case *any:
		var tmp []any
		outVal := reflect.Indirect(reflect.ValueOf(&tmp))
		err := t.ReadArray(buf, arraySliceRead(outVal))
		if err != nil {
			return err
		}
		*out = tmp
		return nil
	}
	outVal := reflect.Indirect(reflect.ValueOf(into))
	switch outVal.Kind() {
	case reflect.Slice:
		return t.ReadArray(buf, arraySliceRead(outVal))
	case reflect.Array:
		return t.ReadArray(buf, arrayArrayRead(outVal))
	case reflect.Map:
		return t.readMap(buf, outVal)
	}
	return cantRead("array", into)
}

func (t *Array) ReadArray(buf *ReadBuffer, ls ArrayUnmarshaler) error {
	revert, len, esz := t.readPrepare(buf)
	defer buf.Unslice(revert)
	if err := ls.VampArrayResize(len); err != nil {
		return fmt.Errorf("reading array: %w", err)
	}
	for i := uint(0); i < len; i++ {
		rdval, err := ls.VampArrayReader(i)
		if err != nil {
			return errAt(t, i, err)
		}
		if err = t.elmType.Read(buf, rdval); err != nil {
			return errAt(t, i, err)
		}
		buf.sliceFix(esz)
	}
	return ls.VampArrayFinish()
}

func (t *Array) ReadMap(buf *ReadBuffer, m any) error {
	mv := reflect.ValueOf(m)
	if mv.Kind() != reflect.Map {
		return fmt.Errorf("cannot read array as map into %T", m)
	}
	return t.readMap(buf, mv)
}

func (t *Array) ReadOpt(buf *ReadBuffer, into any) (ok bool, err error) {
	l := t.Len(buf)
	if l == 0 {
		return false, nil
	}
	rev := t.Index(buf, 0)
	defer buf.Unslice(rev)
	if err = t.elmType.Read(buf, into); err != nil {
		return false, err
	}
	if l > 1 {
		err = fmt.Errorf("illegal optional length: %d", l)
	}
	return true, err
}

func (t *Array) Len(buf *ReadBuffer) uint { return t.lenRange.readFix(buf, 0) }

func (t *Array) Index(buf *ReadBuffer, i uint) (revert uint) {
	// TODO check i against len?!
	esz := sizeOf(t.elmType, buf.Offset())
	off := buf.readOff(t.lenRange.Size())
	off += t.lenRange.Size() + buf.Offset().Size() + i*esz
	return buf.sliceFix(off)
}

func (a *Array) Compare(t Type, opts CompareOpts) (bool, int) {
	b, ok := t.(*Array)
	if !ok {
		return false, 0
	}
	if a.lenRange != b.lenRange {
		return false, 0
	}
	return a.elmType.Compare(b.elmType, opts)
}

func (a *Array) Hash(h hash.Hash, opts CompareOpts) {
	h.Write([]byte{arrayAlt, byte(a.lenRange)})
	a.elmType.Hash(h, opts)
}

func (t *Array) String() string {
	var sb strings.Builder
	sb.WriteByte('[')
	sb.WriteString(t.lenRange.String())
	sb.WriteByte(' ')
	sb.WriteString(t.elmType.String())
	sb.WriteByte(']')
	return sb.String()
}

func (t *Array) PrintTo(w io.Writer) (n int, err error) {
	iw, ok := internal.WIndent(w)
	if ok {
		defer must.RecoverAs(&err)
	}
	n += must.Ret(fmt.Fprintf(iw, "[%s ", t.lenRange.String()))
	n += must.Ret(t.elmType.PrintTo(iw))
	n += must.Ret(io.WriteString(iw, "]"))
	if ok {
		n += must.Ret(fmt.Fprintln(iw))
	}
	return n, nil
}

// TODO reuse where possible
func (t *Array) writePrepare(buf *Buffer, len uint) (revert, esz uint, err error) {
	if err = t.lenRange.writeFix(buf, 0, len); err != nil {
		return 0, 0, fmt.Errorf("writing array: %w", err)
	}
	if err = buf.writeOff(t.lenRange.Size()); err != nil {
		return 0, 0, fmt.Errorf("writing array: %w", err)
	}
	if len == 0 {
		return 0, 0, nil
	}
	esz = sizeOf(t.elmType, buf.Offset())
	return buf.appendSlice(len * esz), esz, nil
}

// TODO reuse where possible
func (t *Array) readPrepare(buf *ReadBuffer) (revert, len, esz uint) {
	len = t.lenRange.readFix(buf, 0)
	lensz := t.lenRange.Size()
	off := buf.readOff(lensz)
	revert = buf.sliceFix(lensz + buf.Offset().Size() + off)
	esz = sizeOf(t.elmType, buf.Offset())
	return revert, len, esz
}

type mapElement struct{ Key, Val any }

func (t *Array) writeMap(buf *Buffer, mv reflect.Value) error {
	l := uint(mv.Len())
	rever, esz, err := t.writePrepare(buf, l)
	switch {
	case err != nil:
		return err
	case l == 0:
		return nil
	}
	defer buf.Unslice(rever)
	i := uint(0)
	for mit := mv.MapRange(); mit.Next(); {
		e := mapElement{Key: mit.Key().Interface(), Val: mit.Value().Interface()}
		if err = t.elmType.Write(buf, e); err != nil {
			return errAt(t, i, err)
		}
		buf.sliceFix(esz)
		i++
	}
	return nil
}

func (t *Array) readMap(buf *ReadBuffer, mv reflect.Value) error {
	if mv.IsNil() {
		mv.Set(reflect.MakeMap(mv.Type()))
	} else {
		mv.Clear()
	}
	rev, len, esz := t.readPrepare(buf)
	defer buf.Unslice(rev)
	for i := uint(0); i < len; i++ {
		var e mapElement
		if err := t.elmType.Read(buf, &e); err != nil {
			return errAt(t, i, err)
		}
		mv.SetMapIndex(reflect.ValueOf(e.Key), reflect.ValueOf(e.Val))
		buf.sliceFix(esz)
	}
	return nil
}

type arrayEmpty struct{}

func (arrayEmpty) VampArrayLen() uint { return 0 }
func (arrayEmpty) VampArrayWriter(i uint) (any, error) {
	return nil, fmt.Errorf("index %d exceeds empty array", i)
}

type arrayOptVal struct{ v any }

func (arrayOptVal) VampArrayLen() uint { return 1 }

func (o arrayOptVal) VampArrayWriter(i uint) (any, error) {
	if i != 0 {
		return nil, fmt.Errorf("index %d exceeds optional", i)
	}
	return o.v, nil
}

type arrayValWrite reflect.Value

func (l arrayValWrite) VampArrayLen() uint {
	return uint(reflect.Value(l).Len())
}

func (l arrayValWrite) VampArrayWriter(i uint) (any, error) {
	return reflect.Value(l).Index(int(i)).Interface(), nil
}

type arrayArrayRead reflect.Value

func (m arrayArrayRead) VampArrayResize(l uint) error {
	val := reflect.Value(m)
	if l != uint(val.Len()) {
		return fmt.Errorf("array len %d does not match value len %d", l, val.Len())
	}
	return nil
}

func (m arrayArrayRead) VampArrayReader(i uint) (any, error) {
	e := reflect.Value(m).Index(int(i))
	if e.Kind() != reflect.Ptr {
		e = e.Addr()
	}
	return e.Interface(), nil
}

func (arrayArrayRead) VampArrayFinish() error { return nil }

type arraySliceRead reflect.Value

func (m arraySliceRead) VampArrayResize(l uint) error {
	val := reflect.Value(m)
	if uint(val.Cap()) >= l {
		val.Set(val.Slice(0, int(l)))
	} else {
		tmp := reflect.MakeSlice(val.Type(), int(l), int(l))
		val.Set(tmp)
	}
	return nil
}

func (m arraySliceRead) VampArrayReader(i uint) (any, error) {
	e := reflect.Value(m).Index(int(i))
	if e.Kind() != reflect.Ptr {
		e = e.Addr()
	}
	return e.Interface(), nil
}

func (arraySliceRead) VampArrayFinish() error { return nil }

func (t *Array) VampRecordWrite(rt *Record, buf *Buffer) error {
	return rt.WriteFields(buf, t.lenRange, t.elmType)
}

func (t *Array) VampRecordRead(rt *Record, buf *ReadBuffer) error {
	err := rt.ReadFields(buf, &t.lenRange, &t.elmType)
	if err != nil {
		return err
	}
	return nil
}

func (t *Array) VampUnionAlt() (uint, any) { return arrayAlt, t }
