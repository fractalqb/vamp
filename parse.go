// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"bufio"
	"fmt"
	"io"
	"strings"
	"unicode"

	"git.fractalqb.de/fractalqb/eloc/must"
)

func ParseType(rd io.Reader) (typ Type, err error) {
	defer must.RecoverAs(&err)
	var br *bufio.Reader
	if tmp, ok := rd.(*bufio.Reader); ok {
		br = tmp
	} else {
		br = bufio.NewReader(rd)
	}
	return parseType(br, lexSikpWS(br)), nil
}

func ParseTypeString(s string) (Type, error) {
	return ParseType(strings.NewReader(s))
}

func parseType(rd *bufio.Reader, tok token) Type {
	if tok.typ() == tokWS {
		tok = lex(rd)
	}
	switch tok.typ() {
	case tokBool:
		return Bool
	case tokInt:
		st := tok.(sizedBasicToken)
		switch st.s {
		case 8:
			return Int8
		case 16:
			return Int16
		case 32:
			return Int32
		case 64:
			return Int64
		default:
			panic(fmt.Errorf("illegal int size: %d", st.s))
		}
	case tokUint:
		st := tok.(sizedBasicToken)
		switch st.s {
		case 8:
			return Uint8
		case 16:
			return Uint16
		case 32:
			return Uint32
		case 64:
			return Uint64
		default:
			panic(fmt.Errorf("illegal uint size: %d", st.s))
		}
	case tokFloat:
		st := tok.(sizedBasicToken)
		switch st.s {
		case 32:
			return Float32
		case 64:
			return Float64
		default:
			panic(fmt.Errorf("illegal float size: %d", st.s))
		}
	case tokAny:
		return Any
	case tokBytes:
		return parseBytes(rd)
	case tokRecStart:
		return parseRecord(rd)
	case tokArrayStart:
		return parseArray(rd)
	case tokUnionStart:
		return parseUnion(rd)
	case tokListStart:
		return parseList(rd)
	}
	panic(fmt.Errorf("illegal token %s", tok))
}

func parseBytes(rd *bufio.Reader) Bytes {
	tok := lex(rd)
	if tok.typ() != tokSizeRange {
		panic(fmt.Errorf("unexpected token %s", tok))
	}
	st := tok.(sizeToken)
	if tok = lex(rd); tok.typ() != tokBytes {
		panic(fmt.Errorf("unterminated bytes type at %s", tok))
	}
	return NewBytes(st.s, BytesReadMode(0))
}

func parseRecord(rd *bufio.Reader) *Record {
	var fields []Type
LOOP:
	for {
		var name string
		tok := lexSikpWS(rd)
		switch tok.typ() {
		case tokRecEnd:
			break LOOP
		case tokName:
			name = tok.(simpleToken).str()
			if tok = lex(rd); tok.typ() != tokNameSep {
				panic(fmt.Errorf("unexpected token %s", tok))
			}
			tok = lex(rd)
		}
		typ := parseType(rd, tok)
		if name == "" {
			fields = append(fields, typ)
		} else {
			fields = append(fields, MustNamed(name, typ))
		}
	}
	return NewRecord(fields...)
}

func parseArray(rd *bufio.Reader) *Array {
	tok := lexSikpWS(rd)
	if tok.typ() != tokSizeRange {
		panic(fmt.Errorf("illegal token %s", tok))
	}
	sr := tok.(sizeToken).s
	typ := parseType(rd, lexSikpWS(rd))
	if tok = lexSikpWS(rd); tok.typ() != tokArrayEnd {
		panic(fmt.Errorf("illegal token %s", tok))
	}
	return NewArray(sr, typ)
}

func parseUnion(rd *bufio.Reader) *Union {
	tok := lexSikpWS(rd)
	if tok.typ() != tokSizeRange {
		panic(fmt.Errorf("illegal token %s", tok))
	}
	ar := tok.(sizeToken).s
	var alts []Type
LOOP:
	for {
		if len(alts) == 0 {
			tok = lexSikpWS(rd)
		} else {
			if tok = lexSikpWS(rd); tok.typ() == tokUnionEnd {
				break LOOP
			} else if tok.typ() != tokUnionSep {
				panic(fmt.Errorf("illegal token %s", tok))
			}
			tok = lexSikpWS(rd)
		}
		var name string
		if tok.typ() == tokName {
			name = tok.(simpleToken).str()
			if tok = lex(rd); tok.typ() != tokNameSep {
				panic(fmt.Errorf("illegal token %s", tok))
			}
			tok = lex(rd)
		}
		typ := parseType(rd, tok)
		if name == "" {
			alts = append(alts, typ)
		} else {
			alts = append(alts, MustNamed(name, typ))
		}
	}
	// TODO Check ar <-> len(alts)
	return NewUnion(ar, alts...)
}

func parseList(rd *bufio.Reader) *List {
	tok := lexSikpWS(rd)
	if tok.typ() != tokSizeRange {
		panic(fmt.Errorf("illegal token %s", tok))
	}
	sr := tok.(sizeToken).s
	typ := parseType(rd, lexSikpWS(rd))
	if tok = lexSikpWS(rd); tok.typ() != tokListEnd {
		panic(fmt.Errorf("illegal token %s", tok))
	}
	return NewList(sr, typ)
}

type token interface {
	typ() tokType
	str() string
}

//go:generate stringer -type tokType
type tokType int

const (
	tokWS tokType = iota
	tokBool
	tokInt
	tokUint
	tokFloat
	tokAny
	tokBytes
	tokArrayStart
	tokArrayEnd
	tokRecStart
	tokRecEnd
	tokUnionStart
	tokUnionEnd
	tokUnionSep
	tokListStart
	tokListEnd
	tokSizeRange
	tokName
	tokNameSep
)

type simpleToken struct {
	t tokType
	s string
}

func (t simpleToken) typ() tokType { return t.t }
func (t simpleToken) str() string  { return t.s }
func (t simpleToken) String() string {
	return fmt.Sprintf("%s:%s", t.typ(), t.str())
}

type sizeToken struct {
	t tokType
	s SizeRange
}

func (t sizeToken) typ() tokType { return t.t }
func (t sizeToken) str() string  { return t.s.String() }
func (t sizeToken) String() string {
	return fmt.Sprintf("%s:%s", t.typ(), t.s)
}

type sizedBasicToken struct {
	t tokType
	s int
}

func (t sizedBasicToken) typ() tokType { return t.t }
func (t sizedBasicToken) str() string  { return fmt.Sprintf("sized %s:%d", t.t, t.s) }
func (t sizedBasicToken) String() string {
	return fmt.Sprintf("%s:%d", t.typ(), t.s)
}

func lexSikpWS(rd *bufio.Reader) token {
	tok := lex(rd)
	if tok.typ() == tokWS {
		tok = lex(rd)
	}
	return tok
}

// A nice opportunity to try out Rob Pikes lexer? See:
// https://go.dev/talks/2011/lex.slide or
// https://www.youtube.com/watch?v=HxaD_trXwRE
// -> No, syntax is too simple
func lex(rd *bufio.Reader) token {
	r := mustReadRune(rd)
	if unicode.IsSpace(r) {
		rd.UnreadRune()
		ws := collect(rd, unicode.IsSpace)
		return simpleToken{t: tokWS, s: ws}
	}
	switch r {
	case '"':
		return simpleToken{t: tokBytes, s: string(r)}
	case '[':
		return simpleToken{t: tokArrayStart, s: string(r)}
	case ']':
		return simpleToken{t: tokArrayEnd, s: string(r)}
	case '{':
		return simpleToken{t: tokRecStart, s: string(r)}
	case '}':
		return simpleToken{t: tokRecEnd, s: string(r)}
	case '<':
		return simpleToken{t: tokUnionStart, s: string(r)}
	case '>':
		return simpleToken{t: tokUnionEnd, s: string(r)}
	case '(':
		return simpleToken{t: tokListStart, s: string(r)}
	case ')':
		return simpleToken{t: tokListEnd, s: string(r)}
	case '|':
		return simpleToken{t: tokUnionSep, s: string(r)}
	case ':':
		return simpleToken{t: tokNameSep, s: string(r)}
	default:
		rd.UnreadRune()
		n := name(rd)
		kwTok := keyword[n]
		if kwTok != nil {
			return kwTok
		} else if n != "" {
			return simpleToken{t: tokName, s: n}
		}
	}
	panic(fmt.Errorf("syntax error")) // TODO be more precise
}

var keyword = map[string]token{
	"bool":          simpleToken{t: tokBool, s: "bool"},
	"int8":          sizedBasicToken{t: tokInt, s: 8},
	"int16":         sizedBasicToken{t: tokInt, s: 16},
	"int32":         sizedBasicToken{t: tokInt, s: 32},
	"int64":         sizedBasicToken{t: tokInt, s: 64},
	"uint8":         sizedBasicToken{t: tokUint, s: 8},
	"uint16":        sizedBasicToken{t: tokUint, s: 16},
	"uint32":        sizedBasicToken{t: tokUint, s: 32},
	"uint64":        sizedBasicToken{t: tokUint, s: 64},
	"float32":       sizedBasicToken{t: tokFloat, s: 32},
	"float64":       sizedBasicToken{t: tokFloat, s: 64},
	"any":           simpleToken{t: tokAny, s: "any"},
	Size8.String():  sizeToken{t: tokSizeRange, s: Size8},
	Size16.String(): sizeToken{t: tokSizeRange, s: Size16},
	Size32.String(): sizeToken{t: tokSizeRange, s: Size32},
}

func name(rd *bufio.Reader) string {
	var sb strings.Builder
	for {
		r, _, err := rd.ReadRune()
		switch {
		case err == io.EOF:
			return sb.String()
		case err != nil:
			panic(err)
		}
		if isName(r) {
			sb.WriteRune(r)
		} else {
			rd.UnreadRune()
			return sb.String()
		}
	}
}

func collect(rd *bufio.Reader, filter func(rune) bool) string {
	var sb strings.Builder
	for {
		r := mustReadRune(rd)
		if !filter(r) {
			rd.UnreadRune()
			return sb.String()
		}
		sb.WriteRune(r)
	}
}

func mustReadRune(rd *bufio.Reader) rune {
	r, _, err := rd.ReadRune()
	if err != nil {
		panic(err)
	}
	return r
}

func isName(r rune) bool {
	if unicode.In(r, unicode.Letter, unicode.Digit) {
		return true
	}
	return r == '-' || r == '_'
}
