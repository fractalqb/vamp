// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"fmt"
	"strings"
)

type PathElement struct {
	T   Type
	Idx uint
}

type PathError struct {
	e error
	p []PathElement
}

func (e PathError) Unwrap() error { return e.e }

func (e PathError) Error() string {
	var sb strings.Builder
	for i := len(e.p) - 1; i >= 0; i-- {
		pe := e.p[i]
		switch pe.T.(type) {
		case *Record:
			fmt.Fprintf(&sb, ".%d", pe.Idx)
		case *Array:
			fmt.Fprintf(&sb, "#%d", pe.Idx)
		case *Union:
			fmt.Fprintf(&sb, "|%d", pe.Idx)
		default:
			fmt.Fprintf(&sb, "/%d", pe.Idx)
		}
	}
	sb.WriteByte(':')
	sb.WriteString(e.e.Error())
	return sb.String()
}

func errAt(t Type, i uint, err error) error {
	if pe, ok := err.(PathError); ok {
		return PathError{e: pe.e, p: append(pe.p, PathElement{t, i})}
	}
	return PathError{e: err, p: []PathElement{{t, i}}}
}
