// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"bytes"
	"fmt"
	"hash"
	"io"
	"unicode/utf8"
)

type BytesReadMode uint8

// # Bytes Reading Mode
//
// The bytes mode controls how a Vampire array is unmarshaled when the target
// type is not determined e.g., read into an *any.
const (
	// Bytes will be read as string if the bytes are valid UTF-8
	CheckUTF8 BytesReadMode = iota

	// Bytes will be read as []byte
	ReadRaw

	// Bytes will be read as string. It is not checked if bytes are valid UTF-8.
	ReadString
)

// mode is a vamp but not a Vampire thing (Hash; Equals)
type Bytes struct {
	szRange SizeRange
	mode    BytesReadMode
}

func NewBytes(size SizeRange, mode BytesReadMode) Bytes {
	return Bytes{szRange: size, mode: mode}
}

func NewString(size SizeRange) Bytes { return NewBytes(size, ReadString) }

func (t Bytes) LenRange() SizeRange { return t.szRange }

func (t Bytes) FixSize() uint { return t.szRange.Size() }

func (t Bytes) DynNo() uint { return 1 }

// User Defined Marshaling
//
// Marshaling to byte array:
//
//	interface{ VampToBytes() ([]byte, error) }
//
// Marshaling to string:
//
//	interface{ VampToString() (string, error) }
func (t Bytes) Write(buf *Buffer, value any) error {
	switch value := value.(type) {
	case interface{ VampToString() (string, error) }:
		v, err := value.VampToString()
		if err != nil {
			return err
		}
		return t.WriteString(buf, v)
	case interface{ VampToBytes() ([]byte, error) }:
		v, err := value.VampToBytes()
		if err != nil {
			return err
		}
		return t.WriteBytes(buf, v)
	case string:
		return t.WriteString(buf, value)
	case *string:
		return t.WriteString(buf, *value)
	case []byte:
		return t.WriteBytes(buf, value)
	}
	return cantWrite(value, "string")
}

func (t Bytes) WriteBytes(buf *Buffer, data []byte) error {
	err := t.szRange.writeFix(buf, 0, uint(len(data)))
	if err != nil {
		return fmt.Errorf("writing byes: %w", err)
	}
	err = buf.writeOff(t.szRange.Size())
	if err != nil {
		return fmt.Errorf("writing byes: %w", err)
	}
	buf.append(data)
	return nil
}

func (t Bytes) WriteString(buf *Buffer, s string) error {
	err := t.szRange.writeFix(buf, 0, uint(len(s)))
	if err != nil {
		return fmt.Errorf("writing string: %w", err)
	}
	err = buf.writeOff(t.szRange.Size())
	if err != nil {
		return fmt.Errorf("writing string: %w", err)
	}
	buf.appendString(s)
	return nil
}

// User Defined Unmarshaling
//
// Unmarshaling from byte array:
//
//	interface{ VampFromBytes([]byte) error } // must copy its argument
//
// Unmarshaling from string:
//
//	interface{ VampFromString(string) error }
func (t Bytes) Read(buf *ReadBuffer, into any) (err error) {
	switch into := into.(type) {
	case interface{ VampFromString(string) error }:
		var v string
		v = t.ReadString(buf)
		err = into.VampFromString(v)
	case interface{ VampFromBytes([]byte) error }:
		var v []byte
		v = t.ReadBytes(buf)
		err = into.VampFromBytes(v)
	case *string:
		*into = t.ReadString(buf)
	case *[]byte:
		tmp := t.ReadBytes(buf)
		*into = bytes.Repeat(tmp, 1)
	case *any:
		tmp := t.ReadBytes(buf)
		switch t.mode {
		case ReadString:
			*into = string(tmp)
		case CheckUTF8:
			if utf8.Valid(tmp) {
				*into = string(tmp)
			} else {
				*into = bytes.Repeat(tmp, 1)
			}
		default:
			*into = bytes.Repeat(tmp, 1)
		}
	default:
		err = cantRead("string", into)
	}
	return err
}

func (t Bytes) ReadBytes(buf *ReadBuffer) []byte {
	len := t.szRange.readFix(buf, 0)
	lensz := t.szRange.Size()
	off := buf.readOff(lensz)
	mem := buf.fixAt(lensz + buf.Offset().Size() + off)
	return mem[:len:len]
}

func (t Bytes) ReadString(buf *ReadBuffer) string {
	len := t.szRange.readFix(buf, 0)
	lensz := t.szRange.Size()
	off := buf.readOff(lensz)
	mem := buf.fixAt(lensz + buf.Offset().Size() + off)
	return string(mem[:len])
}

func (s Bytes) Compare(t Type, _ CompareOpts) (bool, int) {
	switch t := t.(type) {
	case Bytes:
		return true, int(s.szRange - t.szRange)
	case *Bytes:
		return true, int(s.szRange - t.szRange)
	}
	return false, 0
}

func (s Bytes) Hash(h hash.Hash, _ CompareOpts) {
	h.Write([]byte{byte(s.szRange)})
}

func (t Bytes) String() string { return fmt.Sprintf("\"%s\"", t.szRange) }

func (t Bytes) PrintTo(w io.Writer) (int, error) {
	return io.WriteString(w, t.String())
}

func (t Bytes) VampToUint8() (uint8, error) { return uint8(t.szRange), nil }

func (t *Bytes) VampFromUint8(v uint8) error {
	if v >= uint8(sizeRangeNo) {
		return fmt.Errorf("invalid string type: %d", v)
	}
	t.szRange = SizeRange(v)
	return nil
}

func (t Bytes) VampUnionAlt() (uint, any) { return bytesAlt, t }
