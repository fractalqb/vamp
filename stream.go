// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"bytes"
	"errors"
	"io"
	"math"
)

type WriteStream struct {
	w         io.Writer
	typ       *List
	buf       *Buffer
	wrPending bool
	len       uint
}

func (l *List) WriteStream(w io.Writer, offSize SizeRange, buffer []byte) (*WriteStream, error) {
	if _, err := w.Write(buffer); err != nil {
		return nil, err
	}
	buf := NewWriteBuffer(l, offSize, 0, buffer[:0])
	l.lenRange.writeFix(buf, offSize.Size(), 0) // size==0 cannot fail
	ws := &WriteStream{
		w:         w,
		typ:       l,
		buf:       buf,
		wrPending: true,
	}
	return ws, nil
}

func (ws *WriteStream) prepWrite(elem any) error {
	if ws.wrPending {
		if err := ws.flush(false); err != nil {
			return err
		}
	}
	et := ws.typ.Elem()
	ws.buf.Reset(et, ws.buf.Offset(), int(ws.buf.Offset().Size()))
	err := et.Write(ws.buf, elem)
	if err == nil {
		ws.len++
	}
	return err
}

func (ws *WriteStream) Write(elem any) error {
	err := ws.prepWrite(elem)
	ws.wrPending = true
	return err
}

func (ws *WriteStream) WriteWithNext(elem any) error {
	err := ws.prepWrite(elem)
	if err != nil {
		return err
	}
	ws.wrPending = false
	ws.buf.Unslice(0)
	err = ws.buf.writeOff(0)
	if err != nil {
		return err
	}
	_, err = ws.w.Write(ws.buf.AllBytes())
	return err
}

func (ws *WriteStream) WriteLast(elem any) error {
	err := ws.prepWrite(elem)
	if err != nil {
		return err
	}
	ws.wrPending = false
	ws.buf.Unslice(0)
	err = ws.buf.Offset().writeFix(ws.buf, 0, 0)
	if err != nil {
		return err
	}
	_, err = ws.w.Write(ws.buf.AllBytes())
	return err
}

func (ws *WriteStream) Flush() error {
	if ws.wrPending {
		return ws.flush(true)
	}
	return nil
}

func (ws *WriteStream) flush(last bool) error {
	ws.buf.Unslice(0)
	if last {
		if err := ws.buf.Offset().writeFix(ws.buf, 0, 0); err != nil {
			return err
		}
	} else {
		if err := ws.buf.writeOff(0); err != nil {
			return err
		}
	}
	_, err := ws.w.Write(ws.buf.AllBytes())
	return err
}

type ReadStream struct {
	r       io.Reader
	typ     *List
	offsz   SizeRange
	buf     []byte
	offNext uint
	err     error
}

func (l *List) ReadStream(r io.Reader, offSize SizeRange, buffer []byte) *ReadStream {
	return &ReadStream{
		r:     r,
		typ:   l,
		offsz: offSize,
		buf:   buffer[:0],
	}
}

func (rs *ReadStream) Err() error { return rs.err }

// Head returns the list's length hint and the raw head data. If the list's
// actual length is unknown or exceeds the [SizeRange] of the [List], the length
// hint will be -1.
func (rs *ReadStream) Head() (length int64, data []byte, err error) {
	if len(rs.buf) > 0 {
		return 0, nil, errors.New("header requested after list access")
	}
	rs.err = rs.firstRead()
	if rs.offNext == 0 {
		length = 0
	} else {
		l := rs.typ.LenRange().read(rs.buf[rs.offsz.Size():])
		if l == 0 || l > math.MaxInt64 { // >MaxInt64 Impossible w/o vamp.Size64
			length = -1
		} else {
			length = int64(l)
		}
	}
	sz := len(rs.buf)
	return length, rs.buf[:sz:sz], rs.err
}

func (rs *ReadStream) Next() bool {
	if len(rs.buf) == 0 {
		rs.err = rs.firstRead()
		if rs.err != nil {
			return false
		}
	}
	if rs.offNext == 0 {
		return false
	}
	osz := rs.offsz.Size()
	rs.buf = rs.buf[:osz]
	_, rs.err = io.ReadFull(rs.r, rs.buf)
	if rs.err != nil {
		return false
	}
	rs.offNext = rs.offsz.read(rs.buf)
	if rs.offNext == 0 {
		buf := bytes.NewBuffer(rs.buf)
		_, rs.err = io.Copy(buf, rs.r)
		if rs.err != nil {
			return false
		}
		rs.buf = buf.Bytes()
	} else {
		rs.buf = makeLen(rs.buf, osz+rs.offNext)
		_, rs.err = io.ReadFull(rs.r, rs.buf[osz:])
		if rs.err != nil {
			return false
		}
	}
	return true
}

func (rs *ReadStream) Buffer() *ReadBuffer {
	return &ReadBuffer{
		mem:   rs.buf,
		offsz: rs.offsz,
		start: rs.offsz.Size(),
	}
}

func (rs *ReadStream) Read(into any) error {
	rbuf := rs.Buffer()
	rs.err = rs.typ.Elem().Read(rbuf, into)
	return rs.err
}

func (rs *ReadStream) firstRead() error {
	headSz := rs.offsz.Size() + rs.typ.lenRange.Size()
	rs.buf = makeLen(rs.buf, headSz)
	_, err := io.ReadFull(rs.r, rs.buf)
	if err != nil {
		return err
	}
	rs.offNext = rs.offsz.read(rs.buf)
	return nil
}
