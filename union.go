// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"fmt"
	"hash"
	"io"
	"strings"

	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/vamp/internal"
)

type UnionMarshaler interface {
	VampUnionAlt() (uint, any)
}

type UnionUnmarshaler interface {
	VampUnionReader(alt uint, into any) (any, error)
}

type Alt struct {
	S uint
	V any
}

func (a Alt) VampUnionAlt() (uint, any) { return a.S, a.V }

type Union struct {
	// When not nil it will be used in [Union.Read] to select the read target
	// depending on the union's alt field. The original into argument of the
	// Read method is passed to AltTarget and the returend reader then is used
	// to read the alt type into. If reader is nil the union is skipped.
	AltTarget func(alt uint, into any) (reader any, err error)

	alts    []Type
	names   []string
	str     string // TODO Weak Ptr (https://github.com/golang/go/issues/67552)
	szRange SizeRange
}

func NewUnion(altRange SizeRange, alts ...Type) *Union {
	res := &Union{alts: alts, szRange: altRange}
	for i, t := range alts {
		if n := TypeName(alts[i]); n != "" {
			res.setAltName(i, n)
		}
		alts[i] = Anonymized(t)
	}
	return res
}

func (t *Union) SetAltReader(f func(uint, any) (any, error)) *Union {
	t.AltTarget = f
	return t
}

func (t *Union) AltRange() SizeRange { return t.szRange }

func (t *Union) NumAlts() uint { return uint(len(t.alts)) }

func (t *Union) Alt(i uint) Type { return t.alts[i] }

func (t *Union) AltName(i uint) string {
	if i < uint(len(t.names)) {
		return t.names[i]
	}
	return ""
}

func (t *Union) setAltName(i int, n string) {
	if i < len(t.names) {
		t.names[i] = n
		return
	}
	if i < cap(t.names) {
		t.names = t.names[:i+1]
	} else {
		t.names = append(t.names, make([]string, i+1-len(t.names))...)
	}
	t.names[i] = n
}

func (t *Union) FixSize() uint { return t.szRange.Size() }

func (t *Union) DynNo() uint { return 1 }

func (t *Union) Write(buf *Buffer, value any) error {
	if m, ok := value.(UnionMarshaler); ok {
		s, v := m.VampUnionAlt()
		return t.WriteAlt(buf, s, v)
	}
	return cantWrite(value, "union")
}

func (t *Union) WriteAlt(buf *Buffer, alt uint, value any) error {
	if alt > uint(len(t.alts)) {
		return fmt.Errorf("invalid alt %d for write", alt)
	}
	err := t.szRange.writeFix(buf, 0, alt)
	if err != nil {
		return errAt(t, alt, err)
	}
	err = buf.writeOff(t.szRange.Size())
	if err != nil {
		return errAt(t, alt, err)
	}
	at := t.alts[alt]
	rev := buf.appendSlice(sizeOf(at, buf.Offset()))
	err = at.Write(buf, value)
	buf.Unslice(rev)
	if err != nil {
		return errAt(t, alt, err)
	}
	return err
}

func (t *Union) Read(buf *ReadBuffer, into any) error {
	alt := t.szRange.readFix(buf, 0)
	if alt > uint(len(t.alts)) {
		return fmt.Errorf("read invalid alt %d", alt)
	}
	at := t.alts[alt]
	altsz := t.szRange.Size()
	off := buf.readOff(altsz)
	rev := buf.sliceFix(altsz + buf.Offset().Size() + off)
	defer buf.Unslice(rev)
	if m, ok := into.(UnionUnmarshaler); ok {
		rd, err := m.VampUnionReader(alt, into)
		if err != nil {
			return errAt(t, alt, err)
		}
		if err = at.Read(buf, rd); err != nil {
			return errAt(t, alt, err)
		}
	} else if t.AltTarget != nil {
		rd, err := t.AltTarget(alt, into)
		if err != nil {
			return errAt(t, alt, err)
		}
		if rd == nil {
			return nil
		}
		if err = at.Read(buf, rd); err != nil {
			return errAt(t, alt, err)
		}
	} else if err := at.Read(buf, into); err != nil {
		return errAt(t, alt, err)
	}
	return nil
}

func (t *Union) ReadAlt(buf *ReadBuffer, altInto func(uint) (any, error)) (any, error) {
	alt := t.szRange.readFix(buf, 0)
	if alt > uint(len(t.alts)) {
		return nil, fmt.Errorf("read invalid alt %d", alt)
	}
	at := t.alts[alt]
	altsz := t.szRange.Size()
	off := buf.readOff(altsz)
	into, err := altInto(alt)
	switch {
	case err != nil:
		return nil, fmt.Errorf("value target for alt %d: %w", alt, err)
	case into == nil:
		return nil, nil
	}
	rev := buf.sliceFix(altsz + buf.Offset().Size() + off)
	err = at.Read(buf, into)
	buf.Unslice(rev)
	if err != nil {
		return nil, errAt(t, alt, err)
	}
	return into, err
}

func (u *Union) Compare(t Type, opts CompareOpts) (bool, int) {
	v, ok := t.(*Union)
	if !ok {
		return false, 0
	}
	ano := uint(len(u.alts) - len(v.alts))
	if opts&AltNames != 0 {
		for i := range ano {
			if u.AltName(i) != v.AltName(i) {
				return false, 0
			}
		}
	}
	cmp := len(u.alts) - len(v.alts)
	for i := range ano {
		ok, acmp := u.Alt(i).Compare(v.Alt(i), opts)
		switch {
		case !ok:
			return false, 0
		case acmp == 0:
			continue
		case acmp < 0:
			if cmp > 0 {
				return false, 0
			}
			cmp = -1
		case acmp > 0:
			if cmp < 0 {
				return false, 0
			}
			cmp = 1
		}
	}
	return true, cmp
}

func (u *Union) Hash(h hash.Hash, opts CompareOpts) {
	h.Write([]byte{unionAlt, byte(u.szRange)})
	if opts&AltNames != 0 {
		for i, at := range u.alts {
			io.WriteString(h, u.AltName(uint(i)))
			at.Hash(h, opts)
		}
	} else {
		for _, at := range u.alts {
			at.Hash(h, opts)
		}
	}
}

func (t *Union) String() string {
	if t.str == "" {
		var sb strings.Builder
		sb.WriteByte('<')
		sb.WriteString(t.szRange.String())
		sb.WriteByte(' ')
		if len(t.alts) > 0 {
			if n := t.AltName(0); n != "" {
				sb.WriteString(n)
				sb.WriteByte(':')
			}
			sb.WriteString(t.alts[0].String())
			for i := 1; i < len(t.alts); i++ {
				sb.WriteByte('|')
				if n := t.AltName(uint(i)); n != "" {
					sb.WriteString(n)
					sb.WriteByte(':')
				}
				sb.WriteString(t.alts[i].String())
			}
		}
		sb.WriteByte('>')
		t.str = sb.String()
	}
	return t.str
}

func (t *Union) PrintTo(w io.Writer) (n int, err error) {
	if len(t.alts) == 0 {
		return fmt.Fprintf(w, "<%s>", t.szRange.String())
	}
	iw, ok := internal.WIndent(w)
	if ok {
		defer must.RecoverAs(&err)
	}
	n += must.Ret(fmt.Fprintf(iw, "<%s\n", t.szRange.String()))
	iw.Indent(1)
	for i := range t.alts {
		if i == 0 {
			n += must.Ret(io.WriteString(iw, "  "))
		} else {
			n += must.Ret(io.WriteString(iw, "| "))
		}
		if name := t.AltName(uint(i)); name != "" {
			n += must.Ret(fmt.Fprintf(iw, "%s: ", name))
		}
		iw.Indent(2)
		n += must.Ret(t.alts[i].PrintTo(iw))
		n += must.Ret(fmt.Fprintln(iw))
		iw.Indent(-2)
	}
	iw.Indent(-1)
	n += must.Ret(io.WriteString(iw, ">"))
	if ok {
		n += must.Ret(fmt.Fprintln(iw))
	}
	return n, nil
}

func (t *Union) VampUnionAlt() (uint, any) { return unionAlt, t }

func (t *Union) VampRecordWrite(rt *Record, buf *Buffer) error {
	return rt.WriteFields(buf, t.szRange, unnAltsWr{t})
}

type unnAltsWr struct{ ut *Union }

func (wr unnAltsWr) VampArrayLen() uint { return wr.ut.NumAlts() }

func (wr unnAltsWr) VampArrayWriter(i uint) (any, error) {
	return unnAltWr{wr.ut.AltName(i), wr.ut.alts[i]}, nil
}

type unnAltWr struct {
	n string
	t Type
}

func (wr unnAltWr) VampRecordWrite(rt *Record, buf *Buffer) error {
	return rt.WriteFields(buf, wr.n, wr.t)
}

func (t *Union) VampRecordRead(rt *Record, buf *ReadBuffer) error {
	return rt.ReadFields(buf, &t.szRange, unnAltsRd{t})
}

type unnAltsRd struct{ ut *Union }

func (rd unnAltsRd) VampArrayResize(l uint) error {
	if l > uint(cap(rd.ut.alts)) {
		rd.ut.alts = make([]Type, l)
	} else {
		rd.ut.alts = rd.ut.alts[:l]
	}
	return nil
}

func (rd unnAltsRd) VampArrayReader(i uint) (any, error) {
	return unnAltRd{rd.ut, i}, nil
}

func (rd unnAltsRd) VampArrayFinish() error { return nil }

type unnAltRd struct {
	u *Union
	a uint
}

func (rd unnAltRd) VampRecordRead(rt *Record, buf *ReadBuffer) error {
	var n string
	if err := rt.ReadFields(buf, &n, &rd.u.alts[rd.a]); err != nil {
		return err
	}
	if n != "" {
		rd.u.setAltName(int(rd.a), n)
	}
	return nil
}
