// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"fmt"
)

func ExampleUnion() {
	type address struct {
		Street string
		No     string
	}
	type geocoos struct {
		Lat, Lon float64
	}
	addrType, _, _ := ReflectType(address{})
	geoType, _, _ := ReflectType(geocoos{})
	placeType := NewUnion(Size8,
		addrType,
		MustNamed("geo", geoType),
	)
	fmt.Println(placeType)
	buf := NewWriteBuffer(placeType, Size16, 0, nil)
	placeType.Write(buf, Alt{S: 0, V: address{Street: "Justroad", No: "33a"}})
	var out any
	placeType.Read(&buf.ReadBuffer, &out)
	fmt.Printf("%[1]T=%[1]v\n", out)
	buf.Reset(placeType, Size16, 0)
	placeType.Write(buf, Alt{S: 1, V: geocoos{Lat: 33.321, Lon: 44.123}})
	placeType.Read(&buf.ReadBuffer, &out)
	fmt.Printf("%[1]T=%[1]v\n", out)
	// Output:
	// <Size8 {Street:"Size16" No:"Size16"}|geo:{Lat:float64 Lon:float64}>
	// map[string]interface {}=map[No:33a Street:Justroad]
	// map[string]interface {}=map[Lat:33.321 Lon:44.123]
}
