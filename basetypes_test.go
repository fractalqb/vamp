// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"math"
	"testing"
	"testing/quick"

	"golang.org/x/exp/constraints"
)

var (
	_ Type = Bool
	_ Type = Int8
	_ Type = Int16
	_ Type = Int32
	_ Type = Int64
	_ Type = Uint8
	_ Type = Uint16
	_ Type = Uint32
	_ Type = Uint64
	_ Type = Float32
	_ Type = Float64

	_ UnionMarshaler = Bool
	_ UnionMarshaler = Int8
	_ UnionMarshaler = Int16
	_ UnionMarshaler = Int32
	_ UnionMarshaler = Int64
	_ UnionMarshaler = Uint8
	_ UnionMarshaler = Uint16
	_ UnionMarshaler = Uint32
	_ UnionMarshaler = Uint64
	_ UnionMarshaler = Float32
	_ UnionMarshaler = Float64
)

func TestBoolType(t *testing.T) {
	buf := NewWriteBuffer(Bool, Size8, 0, nil)
	err := Bool.Write(buf, true)
	if err != nil {
		t.Fatal(err)
	}
	var b bool
	err = Bool.Read(&buf.ReadBuffer, &b)
	if err != nil {
		t.Fatal(err)
	}
	if !b {
		t.Error("unexpected false")
	}
	err = Bool.Write(buf, false)
	if err != nil {
		t.Fatal(err)
	}
	err = Bool.Read(&buf.ReadBuffer, &b)
	if err != nil {
		t.Fatal(err)
	}
	if b {
		t.Error("unexpected true")
	}
}

func intsTestWriteRead[T constraints.Integer](t *testing.T, typ Type, rngs ...T) {
	t.Run("write & read "+typ.String(), func(t *testing.T) {
		var buf Buffer
		r := 0
		for r+1 < len(rngs) {
			i := rngs[r]
			for {
				buf.Reset(typ, Size8, 0)
				err := typ.Write(&buf, i)
				if err != nil {
					t.Fatal(err)
				}
				var j T
				err = typ.Read(&buf.ReadBuffer, &j)
				if err != nil {
					t.Fatal(err)
				}
				if i != j {
					t.Errorf("%d != %d", i, j)
				}
				if i == rngs[r+1] {
					break
				}
				i++
			}
			r += 2
		}
	})
}

func TestInts(t *testing.T) {
	intsTestWriteRead[int8](t, Int8,
		math.MinInt8, math.MaxInt8,
	)
	intsTestWriteRead[int16](t, Int16,
		math.MinInt16, math.MinInt16+5,
		-5, 5,
		math.MaxInt16-5, math.MaxInt16,
	)
	intsTestWriteRead[int32](t, Int32,
		math.MinInt32, math.MinInt32+5,
		-5, 5,
		math.MaxInt32-5, math.MaxInt32,
	)
	intsTestWriteRead[int64](t, Int64,
		math.MinInt64, math.MinInt64+5,
		-5, 5,
		math.MaxInt64-5, math.MaxInt64,
	)
	intsTestWriteRead[uint8](t, Uint8,
		0, math.MaxUint8,
	)
	intsTestWriteRead[uint16](t, Uint16,
		0, 5,
		math.MaxUint16-5, math.MaxUint16,
	)
	intsTestWriteRead[uint32](t, Uint32,
		0, 5,
		math.MaxUint32-5, math.MaxUint32,
	)
	intsTestWriteRead[uint64](t, Uint64,
		0, 5,
		math.MaxUint64-5, math.MaxUint64,
	)
}

func TestFloat32Type(t *testing.T) {
	var buf Buffer
	err := quick.Check(func(x float32) bool {
		buf.Reset(Float32, Size8, 0)
		err := Float32.Write(&buf, x)
		if err != nil {
			t.Log(err)
			return false
		}
		var y float32
		err = Float32.Read(&buf.ReadBuffer, &y)
		if err != nil {
			t.Log(err)
			return false
		}
		return x == y
	}, nil)
	if err != nil {
		t.Error(err)
	}
}

func TestFloat64Type(t *testing.T) {
	var buf Buffer
	err := quick.Check(func(x float64) bool {
		buf.Reset(Float64, Size8, 0)
		err := Float64.Write(&buf, x)
		if err != nil {
			t.Log(err)
			return false
		}
		var y float64
		err = Float64.Read(&buf.ReadBuffer, &y)
		if err != nil {
			t.Log(err)
			return false
		}
		return x == y
	}, nil)
	if err != nil {
		t.Error(err)
	}
}
