// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"fmt"
)

func ExamplePathError() {
	rt := NewRecord(
		NewString(Size16),
		Float64,
		Bool,
	)
	buf := NewWriteBuffer(rt, Size16, 0, nil)
	err := rt.WriteFields(buf, "next fails", "", true)
	fmt.Println(err)
	// Output:
	// .1:vampire cannot write string as float64
}
