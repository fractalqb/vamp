// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"fmt"
	"hash"
	"io"
	"reflect"
	"strings"
	"sync"

	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/vamp/internal"
)

type RecordMarshaler interface {
	VampRecordWrite(*Record, *Buffer) error
}

type RecordUnmarshaler interface {
	VampRecordRead(*Record, *ReadBuffer) error
}

// TODO Slice to field
type Record struct {
	fields []Type
	fixsz  uint
	dynno  uint
	names  []string
	str    string // TODO Weak Ptr (https://github.com/golang/go/issues/67552)
}

func NewRecord(fields ...Type) *Record {
	if len(fields) == 0 {
		// ListType requires each value to be > 0 size
		return nil
	}
	res := &Record{fields: fields}
	for i, f := range fields {
		res.fixsz += f.FixSize()
		res.dynno += f.DynNo()
		if n := TypeName(f); n != "" {
			res.setFieldName(i, n)
		}
		res.fields[i] = Anonymized(res.fields[i])
	}
	return res
}

func (t *Record) NumField() uint { return uint(len(t.fields)) }

func (t *Record) FieldName(i uint) string {
	if i < uint(len(t.names)) {
		return t.names[i]
	}
	return ""
}

func (t *Record) setFieldName(i int, n string) {
	if i < len(t.names) {
		t.names[i] = n
		return
	}
	if i < cap(t.names) {
		t.names = t.names[:i+1]
	} else {
		t.names = append(t.names, make([]string, i+1-len(t.names))...)
	}
	t.names[i] = n
}

func (t *Record) HasNames() bool { return len(t.names) > 0 }

func (t *Record) FieldType(i uint) Type {
	if i < uint(len(t.fields)) {
		return t.fields[i]
	}
	return nil
}

func (t *Record) Field(buf *ReadBuffer, i uint) (revert uint) {
	// TODO precompute offsets
	off := uint(0)
	for j := uint(0); j < i; j++ {
		off += sizeOf(t.fields[j], buf.Offset())
	}
	return buf.sliceFix(off)
}

func (t *Record) FixSize() uint { return t.fixsz }

func (t *Record) DynNo() uint { return t.dynno }

func (t *Record) Write(buf *Buffer, value any) error {
	if m, ok := value.(RecordMarshaler); ok {
		return m.VampRecordWrite(t, buf)
	}
	val := reflect.Indirect(reflect.ValueOf(value))
	switch val.Kind() {
	case reflect.Slice, reflect.Array:
		vls := sliceVals(val)
		return t.WriteFields(buf, vls...)
	case reflect.Struct:
		var vls []any
		if tmp, ok := fieldsCache.Load(reflect.TypeOf(value)); ok {
			fs := tmp.(Fields)
			vls = fs.Get(value, nil)
		} else {
			vls = structVals(val, nil)
		}
		return t.WriteFields(buf, vls...)
	}
	return cantWrite(value, "record")
}

func sliceVals(v reflect.Value) []any {
	ls := make([]any, v.Len())
	for i := 0; i < v.Len(); i++ {
		ls[i] = v.Index(i).Interface()
	}
	return ls
}

func structVals(v reflect.Value, ls []any) []any {
	t := v.Type()
	for i := 0; i < v.NumField(); i++ {
		if f := t.Field(i); !f.IsExported() {
			continue
		} else if f.Anonymous {
			ls = structVals(v.Field(i), ls)
		} else {
			ls = append(ls, v.Field(i).Interface())
		}
	}
	return ls
}

func (t *Record) WriteFields(buf *Buffer, values ...any) error {
	if len(t.fields) != len(values) {
		return fmt.Errorf("writing %d values for record with %d fields", len(values), len(t.fields))
	}
	if len(values) == 0 {
		return nil
	}
	ft := t.fields[0]
	err := ft.Write(buf, values[0])
	if err != nil {
		return errAt(t, 0, err)
	}
	if len(values) > 1 {
		esz := sizeOf(ft, buf.Offset())
		rev := buf.sliceFix(esz)
		defer buf.Unslice(rev)
		for i := 1; i < len(values); i++ {
			ft = t.fields[i]
			if err = ft.Write(buf, values[i]); err != nil {
				return errAt(t, uint(i), err)
			}
			esz = sizeOf(ft, buf.Offset())
			buf.sliceFix(esz)
		}
	}
	return nil
}

func (t *Record) Read(buf *ReadBuffer, out any) (err error) {
	switch out := out.(type) {
	case RecordUnmarshaler:
		return out.VampRecordRead(t, buf)
	case *any:
		if t.HasNames() {
			tmp := make(map[string]any)
			if err = t.mapOut(buf, tmp); err != nil {
				return err
			}
			*out = tmp
		} else {
			var s []any
			if err := t.sliceOut(buf, &s); err != nil {
				return err
			}
			*out = s
		}
		return nil
	case map[string]any:
		return t.mapOut(buf, out)
	case *[]any:
		return t.sliceOut(buf, out)
	}
	defer must.RecoverAs(&err)
	ov := reflect.Indirect(reflect.ValueOf(out))
	switch ov.Kind() {
	case reflect.Struct:
		var fields []any
		if tmp, ok := fieldsCache.Load(ov.Type()); ok {
			fs := tmp.(Fields)
			fields = fs.Set(out, nil)
		} else {
			fields = structOut(ov, nil)
		}
		return t.ReadFields(buf, fields...)
	}
	return cantRead("record", out)
}

func (t *Record) sliceOut(buf *ReadBuffer, out *[]any) error {
	l := len(t.fields)
	if cap(*out) >= l {
		*out = (*out)[:l]
		clear((*out)[l:])
	} else {
		*out = make([]any, l)
	}
	ops := make([]any, len(t.fields))
	for i := range *out {
		ops[i] = &(*out)[i]
	}
	err := t.ReadFields(buf, ops...)
	return err
}

func (t *Record) mapOut(buf *ReadBuffer, out map[string]any) error {
	var os []any
	if err := t.sliceOut(buf, &os); err != nil {
		return err
	}
	for i, v := range os {
		fn := t.FieldName(uint(i))
		if fn == "" {
			fn = fmt.Sprintf("#%d", i)
		}
		out[fn] = v
	}
	return nil
}

func structOut(v reflect.Value, ls []any) []any {
	t := v.Type()
	for i := 0; i < v.NumField(); i++ {
		if f := t.Field(i); !f.IsExported() {
			continue
		} else if f.Anonymous {
			ls = structOut(v.Field(i), ls)
		} else {
			fv := v.Field(i)
			if fv.Kind() != reflect.Ptr {
				fv = fv.Addr()
			}
			ls = append(ls, fv.Interface())
		}
	}
	return ls
}

func (t *Record) ReadFields(buf *ReadBuffer, outs ...any) error {
	if len(t.fields) != len(outs) {
		return fmt.Errorf("reading %d values for record with %d fields", len(outs), len(t.fields))
	}
	if len(outs) == 0 {
		return nil
	}
	ft := t.fields[0]
	err := ft.Read(buf, outs[0])
	if err != nil {
		return errAt(t, 0, err)
	}
	if len(outs) > 1 {
		esz := sizeOf(ft, buf.Offset())
		rev := buf.sliceFix(esz)
		defer buf.Unslice(rev)
		for i := 1; i < len(outs); i++ {
			ft = t.fields[i]
			if err = ft.Read(buf, outs[i]); err != nil {
				return errAt(t, uint(i), err)
			}
			esz := sizeOf(ft, buf.Offset())
			buf.sliceFix(esz)
		}
	}
	return nil
}

func (t *Record) VampArrayLen() uint { return uint(len(t.fields)) }

func (t *Record) VampArrayWriter(i uint) (any, error) {
	return recIOfieldWr{
		n: t.FieldName(i),
		t: t.fields[i],
	}, nil
}

var fieldsCache sync.Map

func UseFieldsForStruct(s any, fs Fields) error {
	t := reflect.TypeOf(s)
	if t.Kind() != reflect.Struct {
		return fmt.Errorf("cannot use Fields for type %T", s)
	}
	if fs.Empty() {
		fieldsCache.Delete(t)
	} else if _, ok := fieldsCache.LoadOrStore(t, fs); ok {
		return fmt.Errorf("duplicate fields use for %T", s)
	}
	return nil
}

type recIOfieldWr struct {
	n string
	t Type
}

func (fw recIOfieldWr) VampRecordWrite(t *Record, buf *Buffer) error {
	return t.WriteFields(buf, fw.n, fw.t)
}

func (t *Record) VampArrayResize(l uint) error {
	if l > uint(cap(t.fields)) {
		t.fields = make([]Type, l)
	} else {
		t.fields = t.fields[:l]
	}
	return nil
}

func (t *Record) VampArrayReader(i uint) (any, error) {
	return recIOfieldRd{t, i}, nil
}

type recIOfieldRd struct {
	r *Record
	f uint
}

func (fr recIOfieldRd) VampRecordRead(t *Record, buf *ReadBuffer) error {
	var n string
	if err := t.ReadFields(buf, &n, &fr.r.fields[fr.f]); err != nil {
		return err
	}
	if n != "" {
		fr.r.setFieldName(int(fr.f), n)
	}
	return nil
}

func (t *Record) VampArrayFinish() error {
	t.fixsz = 0
	t.dynno = 0
	for _, f := range t.fields {
		t.fixsz += f.FixSize()
		t.dynno += f.DynNo()
	}
	return nil
}

func (t *Record) VampUnionAlt() (uint, any) { return recordAlt, t }

func (r *Record) Compare(t Type, opts CompareOpts) (bool, int) {
	s, ok := t.(*Record)
	if !ok {
		return false, 0
	}
	fno := uint(min(len(r.fields), len(s.fields)))
	if opts&FieldNames != 0 {
		for i := range fno {
			if r.FieldName(i) != s.FieldName(i) {
				return false, 0
			}
		}
	}
	cmp := len(r.fields) - len(s.fields)
	for i := range fno {
		ok, fcmp := r.FieldType(i).Compare(s.FieldType(i), opts)
		switch {
		case !ok:
			return false, 0
		case fcmp == 0:
			continue
		case fcmp < 0:
			if cmp > 0 {
				return false, 0
			}
			cmp = -1
		case fcmp > 0:
			if cmp < 0 {
				return false, 0
			}
			cmp = 1
		}
	}
	return true, cmp
}

func (r *Record) Hash(h hash.Hash, opts CompareOpts) {
	h.Write([]byte{recordAlt})
	if opts&FieldNames != 0 {
		for i, ft := range r.fields {
			io.WriteString(h, r.FieldName(uint(i)))
			ft.Hash(h, opts)
		}
	} else {
		for _, ft := range r.fields {
			ft.Hash(h, opts)
		}
	}
}

func (t *Record) String() string {
	if t.str == "" {
		var sb strings.Builder
		sb.WriteByte('{')
		if len(t.fields) > 0 {
			if n := t.FieldName(0); n != "" {
				sb.WriteString(n)
				sb.WriteByte(':')
			}
			sb.WriteString(t.fields[0].String())
			for i := 1; i < len(t.fields); i++ {
				sb.WriteByte(' ')
				if n := t.FieldName(uint(i)); n != "" {
					sb.WriteString(n)
					sb.WriteByte(':')
				}
				sb.WriteString(t.fields[i].String())
			}
		}
		sb.WriteByte('}')
		t.str = sb.String()
	}
	return t.str
}

func (t *Record) PrintTo(w io.Writer) (n int, err error) {
	if len(t.fields) == 0 {
		return io.WriteString(w, "{}")
	}
	iw, ok := internal.WIndent(w)
	if ok {
		defer must.RecoverAs(&err)
	}
	n += must.Ret(fmt.Fprintln(iw, "{"))
	iw.Indent(3)
	for i := range t.fields {
		if name := t.FieldName(uint(i)); name != "" {
			n += must.Ret(fmt.Fprintf(iw, "%s: ", name))
		}
		n += must.Ret(t.fields[i].PrintTo(iw))
		n += must.Ret(fmt.Fprintln(iw))
	}
	iw.Indent(-3)
	n += must.Ret(fmt.Fprint(iw, "}"))
	if ok {
		n += must.Ret(fmt.Fprintln(iw))
	}
	return n, nil
}
