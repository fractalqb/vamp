Unfortunately, the good performance of the format does not come as a pure
advantage over other formats. Direct access to parts of a Vampire-encoded
dataset only works if many of the components of the data format have a fixed
size. For basic types, this works because they are only offered in fixed sizes.
However, there is a whole range of basic types, for example integers with 8, 16,
32 and 64 bits. The situation is even more complicated with the abstract data
types `Array`, `Record`, `List` and `Union`. The same applies to the `Bytes` and
the `Any` type.  For all of them, a choice must be made for the size range. It
needs some understanding of the structure of Vampire encoding to make a
reasonable choice.

In addition, the placement of data with size not known a priori, requires it to
be located via an offset. The size of this offset affects the size of the data
set that can be stored. The offset size is independent of the used Vampire types
and is selected for the actual buffer used for writing and reading. Data written
with a certain offset size must also be read with the same offset size.

# Vampire Data Format

- Each values has a fix encoding size greater than zero and zero or more dynamic
  parts.

- The trunk of a value consists of the fix part and an offset for each dynamic
  part – like branches.

- When writing to a buffer, the trunk is placed at fix position and the dynamic
  parts are always appende to the buffer. – Mutating dynamic parts is not
  supported.

- Generally, offsets are placed as close as possible to the end of the trunk.
  This allows offsets to reach further into the dynamic part area.

- The offset is computed as the number of bytes from the end of the offset to
  the start of the dynamic part.

## Basic Type

Basic types, by definition, have no dynamic part and are encoded with their
specific fix size.

- Integer values are encoded little endian. This also includes size ranges.

## Bytes / String Type

```
+--------+-----+ +------+
| length | off | | data |
+--------+-----+ +------+
               `-^
```

## Record Type

```
+-----------------+ +--------------+
| fix             | | dyn          |
+------+---+------+ +---+------+---+
| fix0 | … | fixN | | … | dyn0 | … |
+------+---+------+ +---+------+---+
     `------------------^
```

- `fixI` is the fix part of record field I.

- Given that the type for field I has a dynamic part, `dynI` is the dynamic
  part.

- Order of dynamic parts inside `dyn` depends on the order of writing the fix
  parts.

## Array Type

```
+--------------------------+ +--------------+
| fix                      | | dyn          |
+--------+------+---+------+ +---+------+---+
| length | fix0 | … | fixL | | … | dyn0 | … |
+--------+------+---+------+ +---+------+---+
             `-------------------^
```

- `fixI` is the fix part of element 0 ≤ I ≤ L (L := length - 1)

- Given that the type for element I has a dynamic part, `dynI` is the dynamic
  part.

- Order of dynamic parts inside `dyn` depends on the order of writing the fix
  parts.

## List Type

```
+--------------------+-------------------------------------
| Header             | Body = Sequence of Elements
+------+-------------+---------------------+-----------+---
| off0 | length hint | element 0           | element 1 | …
|      |             +-------------+-------+-----------+---
|      |             | fix 0       | dyn 0 | …
|      |             +------+------+       +---
|      |             | data | off1 |       | …
+------+-------------+------+------+-------+---
       `-------------^             `-------^
```

- When `offN`—the offset to element N—is 0, there is not element N i.e., the
  list ends.

- `off0` is 0 for empty lists; ignore length hint.

  The offset to the next element must be in the fixed part of the current element. because read buffers do not keep track of how many bytes have been read. Otherwise, each element could be marked with a final byte to determine if there is a next element, and a list wouldn't need an offset size range to link elements.

- `length hint` is 0 when list was writte as a stream or when the list's actual
  length exceed the range of the list length's size range.

## Union Type

```
+-----+-----+ +------+
| alt | off | | data |
+-----+-----+ +------+
            `-^
```

## Any Type