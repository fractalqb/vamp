// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"bufio"
	"fmt"
	"strings"
	"testing"
)

func ExampleParseTypeString() {
	typ, _ := ParseTypeString(`<Size8
		Foo:[Size8 "Size8"]
	  | float32
	  | {ID:uint32 Name:"Size16"}
	>`)
	fmt.Println(typ)
	// Output:
	// <Size8 Foo:[Size8 "Size8"]|float32|{ID:uint32 Name:"Size16"}>
}

func TestLexSizeRanges(t *testing.T) {
	for s := SizeRange(0); s < sizeRangeNo; s++ {
		rd := bufio.NewReader(strings.NewReader(s.String()))
		tok := lex(rd)
		if tok == nil {
			t.Errorf("cannot parse size range: %s", s)
		}
	}
}
