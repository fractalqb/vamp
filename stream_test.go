// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"bytes"
	"reflect"
	"strconv"
	"testing"

	"git.fractalqb.de/fractalqb/testerr"
)

func TestWriteStream(t *testing.T) {
	const offSize = Size16
	ls := NewList(Size8, NewString(Size8))
	var buf bytes.Buffer
	t.Run("deferred", func(t *testing.T) {
		ws := testerr.Shall1(ls.WriteStream(&buf, offSize, nil)).BeNil(t)
		testerr.Shall(ws.Write("0")).BeNil(t)
		testerr.Shall(ws.Write("1")).BeNil(t)
		testerr.Shall(ws.Write("2")).BeNil(t)
		ws.Flush()
		var out []string
		rbuf := NewReadBuffer(ls, offSize, buf.Bytes())
		if err := ls.Read(rbuf, &out); err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(out, []string{"0", "1", "2"}) {
			t.Error(out)
		}
	})
	t.Run("immediate", func(t *testing.T) {
		buf.Reset()
		ws := testerr.Shall1(ls.WriteStream(&buf, offSize, nil)).BeNil(t)
		if err := ws.WriteWithNext("0"); err != nil {
			t.Fatal(err)
		}
		if err := ws.WriteWithNext("1"); err != nil {
			t.Fatal(err)
		}
		if err := ws.WriteLast("2"); err != nil {
			t.Fatal(err)
		}
		ws.Flush()
		var out []string
		rbuf := NewReadBuffer(ls, offSize, buf.Bytes())
		if err := ls.Read(rbuf, &out); err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(out, []string{"0", "1", "2"}) {
			t.Error(out)
		}
	})
}

func TestReadStream(t *testing.T) {
	const offSize = Size16
	ls := NewList(Size8, NewString(Size8))
	wbuf := NewWriteBuffer(ls, offSize, 0, nil)
	data := []string{"0", "1", "2"}
	err := ls.Write(wbuf, data)
	if err != nil {
		t.Fatal(err)
	}
	sr := ls.ReadStream(bytes.NewReader(wbuf.Bytes()), offSize, nil)
	for i, expect := range data {
		if !sr.Next() {
			t.Fatalf("early end: %s", sr.Err())
		}
		var out string
		err = sr.Read(&out)
		if err != nil {
			t.Fatalf("%d expected '%s': %s", i, expect, err)
		}
		if out != expect {
			t.Errorf("read '%s' instead of '%s'", out, expect)
		}
	}
	if sr.Next() {
		t.Error("input has extra element")
	} else {
		t.Log(sr.Err())
	}
}

func TestReadStream_Buffer(t *testing.T) {
	const offSize = Size16
	ls := NewList(Size8, NewString(Size8))
	wbuf := NewWriteBuffer(ls, offSize, 0, nil)
	err := ls.Write(wbuf, []string{"0", "1"})
	if err != nil {
		t.Fatal(err)
	}
	sr := ls.ReadStream(bytes.NewReader(wbuf.Bytes()), offSize, nil)
	if l, data, err := sr.Head(); err != nil {
		t.Fatal("cannot read list head:", err)
	} else if !reflect.DeepEqual(data, []byte{1, 0, 2}) {
		t.Fatalf("head: %v", data)
	} else if l != 2 {
		t.Errorf("wrong list length: %d", l)
	}
	if !sr.Next() {
		t.Fatalf("1st next: %s", sr.Err())
	}
	data := sr.Buffer().Bytes()
	if !reflect.DeepEqual(data, []byte{1, 0, 0, 48}) {
		t.Fatalf("1st element: %v", data)
	}
	data = sr.Buffer().AllBytes()
	if !reflect.DeepEqual(data, []byte{4, 0, 1, 0, 0, 48}) {
		t.Fatalf("1nd all: %v", data)
	}
	if !sr.Next() {
		t.Fatalf("2st next: %s", sr.Err())
	}
	data = sr.Buffer().Bytes()
	if !reflect.DeepEqual(data, []byte{1, 0, 0, 49}) {
		t.Fatalf("2nd element: %v", data)
	}
	data = sr.Buffer().AllBytes()
	if !reflect.DeepEqual(data, []byte{0, 0, 1, 0, 0, 49}) {
		t.Fatalf("2nd all: %v", data)
	}
	if sr.Next() {
		t.Fatal("Next after list end")
	}
}

func TestList_writeReadStream(t *testing.T) {
	elemType := NewString(Size8)
	ls := NewList(Size8, elemType)
	var buf bytes.Buffer
	const offSize = Size16
	ws := testerr.Shall1(ls.WriteStream(&buf, offSize, nil)).BeNil(t)
	testerr.Shall(ws.Write("0")).BeNil(t)
	testerr.Shall(ws.Write("1")).BeNil(t)
	testerr.Shall(ws.Write("2")).BeNil(t)
	ws.Flush()

	sr := ls.ReadStream(bytes.NewReader(buf.Bytes()), offSize, nil)
	if l, data, err := sr.Head(); err != nil {
		t.Fatal("cannot read list head:", err)
	} else if !reflect.DeepEqual(data, []byte{1, 0, 0}) {
		t.Fatalf("head: %v", data)
	} else if l != -1 {
		t.Errorf("wrong list length: %d", l)
	}
	idx := 0
	for sr.Next() {
		var str string
		sr.Read(&str)
		if str != strconv.Itoa(idx) {
			t.Error("Read", str)
		}
		idx++
	}
}
