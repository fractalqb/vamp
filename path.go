// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import "fmt"

type Path struct {
	steps []func(*ReadBuffer) error
	typ   Type
}

func NewPath(t Type, p ...uint) (res Path, err error) {
	for i, s := range p {
		switch typ := t.(type) {
		case *Record:
			if s >= typ.NumField() {
				return Path{},
					fmt.Errorf("step %d:[%d] exceeds %d record fields",
						i, s, typ.NumField())
			}
			res.steps = append(res.steps, recordStep{typ, s}.step)
			res.typ = typ.FieldType(s)
		case *Array:
			if s > typ.LenRange().Max() {
				return Path{},
					fmt.Errorf("step %d:[%d] exceeds max array lengd %d",
						i, s, typ.lenRange.Max())
			}
			res.steps = append(res.steps, arrayStep{typ, s}.step)
			res.typ = typ.Elem()
		}
	}
	return res, nil
}

func (p Path) Select(b *ReadBuffer) (rev uint, err error) {
	rev = b.start
	for _, s := range p.steps {
		err = s(b)
		if err != nil {
			b.Unslice(rev)
			return rev, err
		}
	}
	return rev, nil

}

type recordStep struct {
	t *Record
	i uint
}

func (s recordStep) step(b *ReadBuffer) error {
	s.t.Field(b, s.i)
	return nil
}

type arrayStep struct {
	t *Array
	i uint
}

func (s arrayStep) step(b *ReadBuffer) error {
	if l := s.t.Len(b); s.i >= l {
		return fmt.Errorf("array index %d exceeds len %d", s.i, l)
	}
	s.t.Index(b, s.i)
	return nil
}
