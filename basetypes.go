// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"fmt"
	"hash"
	"io"
	"math"
	"time"

	"golang.org/x/exp/constraints"
)

/*
# Vampire Basic Types

Each <N>Type variable implements Type and represents a Vampire type that
corresponds directly to a Go type <n> where <n> is <N> folded to lower case
e.g., Bool corresponds to Go's bool type.

Marshaler and Unmarshaler interfaces are not explicitly defined for basic types.
To implement user defined (un-)marshaling to or from <N>Type  a value has to
implement methods that match the following convention:

The marshaler for writing <n> to <N>Type:

	interface{ VampTo<N>() (<n>, error) }

The unmarshaler for reading <n> from <N>Type:

	interface{ VampFrom<N>(<n>) error }
*/
var (
	Bool    boolType
	Int8    int8Type
	Int16   int16Type
	Int32   int32Type
	Int64   int64Type
	Uint8   uint8Type
	Uint16  uint16Type
	Uint32  uint32Type
	Uint64  uint64Type
	Float32 float32Type
	Float64 float64Type
)

type boolType struct{}

func (boolType) FixSize() uint { return 1 }

func (boolType) DynNo() uint { return 0 }

func (t boolType) Write(buf *Buffer, value any) error {
	switch value := value.(type) {
	case interface{ VampToBool() (bool, error) }:
		v, err := value.VampToBool()
		if err != nil {
			return err
		}
		t.WriteBool(buf, v)
	case bool:
		t.WriteBool(buf, value)
	case *bool:
		t.WriteBool(buf, *value)
	default:
		return cantWrite(value, "bool")
	}
	return nil
}

func (boolType) WriteBool(buf *Buffer, value bool) {
	if value {
		buf.fixAt(0)[0] = 1
	} else {
		buf.fixAt(0)[0] = 0
	}
}

func (t boolType) Read(buf *ReadBuffer, into any) (err error) {
	switch into := into.(type) {
	case interface{ VampFromBool(bool) error }:
		err = into.VampFromBool(t.ReadBool(buf))
	case *bool:
		*into = t.ReadBool(buf)
	case *any:
		*into = t.ReadBool(buf)
	default:
		err = cantRead("bool", into)
	}
	return err
}

func (boolType) ReadBool(buf *ReadBuffer) bool { return buf.fixAt(0)[0] != 0 }

func (boolType) VampUnionAlt() (uint, any) { return boolAlt, &Bool }

func (boolType) Compare(t Type, _ CompareOpts) (bool, int) {
	return sameAs[boolType](t)
}

func (boolType) Hash(h hash.Hash, _ CompareOpts) { h.Write([]byte{boolAlt}) }

func (boolType) String() string { return "bool" }

func (t boolType) PrintTo(w io.Writer) (int, error) {
	return io.WriteString(w, t.String())
}

type int8Type struct{}

func (int8Type) FixSize() uint { return 1 }

func (int8Type) DynNo() uint { return 0 }

func (int8Type) ReadInt8(buf *ReadBuffer) int8 {
	return int8(buf.fixAt(0)[0])
}

func (t int8Type) Read(buf *ReadBuffer, into any) (err error) {
	switch into := into.(type) {
	case interface{ VampFromInt8(int8) error }:
		err = into.VampFromInt8(t.ReadInt8(buf))
	case *int8:
		*into = t.ReadInt8(buf)
	case *any:
		*into = t.ReadInt8(buf)
	case *int:
		*into = int(t.ReadInt8(buf))
	case *uint:
		err = setUintS(into, t.ReadInt8(buf))
	case *int16:
		*into = int16(t.ReadInt8(buf))
	case *int32:
		*into = int32(t.ReadInt8(buf))
	case *int64:
		*into = int64(t.ReadInt8(buf))
	default:
		err = cantRead("int8", into)
	}
	return err
}

func (int8Type) WriteInt8(buf *Buffer, i int8) {
	mem := buf.fixAt(0)
	mem[0] = byte(i)
}

func (t int8Type) Write(buf *Buffer, value any) (err error) {
	switch value := value.(type) {
	case interface{ VampToInt8() (int8, error) }:
		v, err := value.VampToInt8()
		if err != nil {
			return err
		}
		t.WriteInt8(buf, v)
	case int8:
		t.WriteInt8(buf, value)
	case *int8:
		t.WriteInt8(buf, *value)
	default:
		err = wrInts("int8", math.MinInt8, math.MaxInt8, value, buf, t.WriteInt8)
	}
	return err
}

func (int8Type) VampUnionAlt() (uint, any) { return int8Alt, &Int8 }

func (int8Type) Hash(h hash.Hash, _ CompareOpts) { h.Write([]byte{int8Alt}) }

func (int8Type) Compare(t Type, _ CompareOpts) (bool, int) {
	return sameAs[int8Type](t)
}

func (int8Type) String() string { return "int8" }

func (t int8Type) PrintTo(w io.Writer) (int, error) {
	return io.WriteString(w, t.String())
}

type int16Type struct{}

func (int16Type) FixSize() uint { return 2 }

func (int16Type) DynNo() uint { return 0 }

func (t int16Type) Read(buf *ReadBuffer, into any) (err error) {
	switch into := into.(type) {
	case interface{ VampFromInt16(int16) error }:
		into.VampFromInt16(t.ReadInt16(buf))
	case *int16:
		*into = t.ReadInt16(buf)
	case *any:
		*into = t.ReadInt16(buf)
	case *int:
		*into = int(t.ReadInt16(buf))
	case *uint:
		err = setUintS(into, t.ReadInt16(buf))
	case *int32:
		*into = int32(t.ReadInt16(buf))
	case *int64:
		*into = int64(t.ReadInt16(buf))
	default:
		err = cantRead("int16", into)
	}
	return err
}

func (t int16Type) ReadInt16(buf *ReadBuffer) int16 {
	mem := buf.fixAt(0)
	_ = mem[1] // bounds check hint to compiler
	return int16(mem[0]) | int16(mem[1])<<8
}

func (t int16Type) Write(buf *Buffer, value any) (err error) {
	switch value := value.(type) {
	case interface{ VampToInt16() (int16, error) }:
		v, err := value.VampToInt16()
		if err != nil {
			return err
		}
		t.WriteInt16(buf, v)
	case int16:
		t.WriteInt16(buf, value)
	case *int16:
		t.WriteInt16(buf, *value)
	case int8:
		t.WriteInt16(buf, int16(value))
	case *int8:
		t.WriteInt16(buf, int16(*value))
	case uint8:
		t.WriteInt16(buf, int16(value))
	case *uint8:
		t.WriteInt16(buf, int16(*value))
	default:
		err = wrInts("int16", math.MinInt16, math.MaxInt16, value, buf, t.WriteInt16)
	}
	return err
}

func (int16Type) WriteInt16(buf *Buffer, value int16) {
	mem := buf.fixAt(0)
	_ = mem[1] // bounds check hint to compiler
	mem[0] = byte(value)
	mem[1] = byte(value >> 8)
}

func (int16Type) VampUnionAlt() (uint, any) { return int16Alt, &Int16 }

func (int16Type) Compare(t Type, _ CompareOpts) (bool, int) {
	return sameAs[int16Type](t)
}

func (int16Type) Hash(h hash.Hash, _ CompareOpts) { h.Write([]byte{int16Alt}) }

func (int16Type) String() string { return "int16" }

func (t int16Type) PrintTo(w io.Writer) (int, error) {
	return io.WriteString(w, t.String())
}

type int32Type struct{}

func (int32Type) FixSize() uint { return 4 }

func (int32Type) DynNo() uint { return 0 }

func (t int32Type) Read(buf *ReadBuffer, into any) (err error) {
	switch into := into.(type) {
	case interface{ VampFromInt32(int32) error }:
		into.VampFromInt32(t.ReadInt32(buf))
	case *int32:
		*into = t.ReadInt32(buf)
	case *any:
		*into = t.ReadInt32(buf)
	case *int:
		err = setIntS(into, t.ReadInt32(buf))
	case *uint:
		err = setUintS(into, t.ReadInt32(buf))
	case *int64:
		*into = int64(t.ReadInt32(buf))
	default:
		err = cantRead("int32", into)
	}
	return err
}

func (t int32Type) ReadInt32(buf *ReadBuffer) int32 {
	mem := buf.fixAt(0)
	_ = mem[3] // bounds check hint to compiler; see golang.org/issue/14808
	return int32(mem[0]) | int32(mem[1])<<8 | int32(mem[2])<<16 | int32(mem[3])<<24
}

func (t int32Type) Write(buf *Buffer, value any) (err error) {
	switch value := value.(type) {
	case interface{ VampToInt32() (int32, error) }:
		v, err := value.VampToInt32()
		if err != nil {
			return err
		}
		t.WriteInt32(buf, v)
	case int32:
		t.WriteInt32(buf, value)
	case *int32:
		t.WriteInt32(buf, *value)
	case int8:
		t.WriteInt32(buf, int32(value))
	case *int8:
		t.WriteInt32(buf, int32(*value))
	case int16:
		t.WriteInt32(buf, int32(value))
	case *int16:
		t.WriteInt32(buf, int32(*value))
	case uint8:
		t.WriteInt32(buf, int32(value))
	case *uint8:
		t.WriteInt32(buf, int32(*value))
	case uint16:
		t.WriteInt32(buf, int32(value))
	case *uint16:
		t.WriteInt32(buf, int32(*value))
	default:
		err = wrInts("int32", math.MinInt32, math.MaxInt32, value, buf, t.WriteInt32)
	}
	return err
}

func (int32Type) WriteInt32(buf *Buffer, value int32) {
	mem := buf.fixAt(0)
	_ = mem[3] // bounds check hint to compiler
	mem[0] = byte(value)
	mem[1] = byte(value >> 8)
	mem[2] = byte(value >> 16)
	mem[3] = byte(value >> 24)
}

func (int32Type) VampUnionAlt() (uint, any) { return int32Alt, &Int32 }

func (int32Type) Compare(t Type, _ CompareOpts) (bool, int) {
	return sameAs[int32Type](t)
}

func (int32Type) Hash(h hash.Hash, _ CompareOpts) { h.Write([]byte{int32Alt}) }

func (int32Type) String() string { return "int32" }

func (t int32Type) PrintTo(w io.Writer) (int, error) {
	return io.WriteString(w, t.String())
}

type int64Type struct{}

func (int64Type) FixSize() uint { return 8 }

func (int64Type) DynNo() uint { return 0 }

// Also reads *time.Time with millis and *time.Duration
func (t int64Type) Read(buf *ReadBuffer, into any) (err error) {
	switch into := into.(type) {
	case interface{ VampFromInt64(int64) error }:
		into.VampFromInt64(t.ReadInt64(buf))
	case *int64:
		*into = t.ReadInt64(buf)
	case *any:
		*into = t.ReadInt64(buf)
	case *int:
		err = setIntS(into, t.ReadInt64(buf))
	case *uint:
		err = setUintS(into, t.ReadInt64(buf))
	case *time.Time:
		*into = time.UnixMilli(t.ReadInt64(buf))
	case *time.Duration:
		*into = time.Duration(t.ReadInt64(buf))
	default:
		err = cantRead("int64", into)
	}
	return err
}

func (t int64Type) ReadInt64(buf *ReadBuffer) int64 {
	mem := buf.fixAt(0)
	_ = mem[7] // bounds check hint to compiler
	return int64(mem[0]) | int64(mem[1])<<8 | int64(mem[2])<<16 | int64(mem[3])<<24 |
		int64(mem[4])<<32 | int64(mem[5])<<40 | int64(mem[6])<<48 | int64(mem[7])<<56
}

// Also write time.Time with millis and time.Duration and their pointers
func (t int64Type) Write(buf *Buffer, value any) (err error) {
	switch value := value.(type) {
	case interface{ VampToInt64() (int64, error) }:
		v, err := value.VampToInt64()
		if err != nil {
			return err
		}
		t.WriteInt64(buf, v)
	case int64:
		t.WriteInt64(buf, value)
	case *int64:
		t.WriteInt64(buf, *value)
	case time.Time:
		t.WriteInt64(buf, value.UnixMilli())
	case time.Duration:
		t.WriteInt64(buf, int64(value))
	case *time.Time:
		t.WriteInt64(buf, value.UnixMilli())
	case *time.Duration:
		t.WriteInt64(buf, int64(*value))
	case int8:
		t.WriteInt64(buf, int64(value))
	case *int8:
		t.WriteInt64(buf, int64(*value))
	case int16:
		t.WriteInt64(buf, int64(value))
	case *int16:
		t.WriteInt64(buf, int64(*value))
	case int32:
		t.WriteInt64(buf, int64(value))
	case *int32:
		t.WriteInt64(buf, int64(*value))
	case uint8:
		t.WriteInt64(buf, int64(value))
	case *uint8:
		t.WriteInt64(buf, int64(*value))
	case uint16:
		t.WriteInt64(buf, int64(value))
	case *uint16:
		t.WriteInt64(buf, int64(*value))
	case uint32:
		t.WriteInt64(buf, int64(value))
	case *uint32:
		t.WriteInt64(buf, int64(*value))
	default:
		err = wrInts("int64", math.MinInt64, math.MaxInt64, value, buf, t.WriteInt64)
	}
	return err
}

func (int64Type) WriteInt64(buf *Buffer, value int64) {
	mem := buf.fixAt(0)
	_ = mem[7] // bounds check hint to compiler
	mem[0] = byte(value)
	mem[1] = byte(value >> 8)
	mem[2] = byte(value >> 16)
	mem[3] = byte(value >> 24)
	mem[4] = byte(value >> 32)
	mem[5] = byte(value >> 40)
	mem[6] = byte(value >> 48)
	mem[7] = byte(value >> 56)
}

func (int64Type) VampUnionAlt() (uint, any) { return int64Alt, &Int64 }

func (int64Type) Compare(t Type, _ CompareOpts) (bool, int) {
	return sameAs[int64Type](t)
}

func (int64Type) Hash(h hash.Hash, _ CompareOpts) { h.Write([]byte{int64Alt}) }

func (int64Type) String() string { return "int64" }

func (t int64Type) PrintTo(w io.Writer) (int, error) {
	return io.WriteString(w, t.String())
}

type uint8Type struct{}

func (uint8Type) FixSize() uint { return 1 }

func (uint8Type) DynNo() uint { return 0 }

func (uint8Type) ReadUint8(buf *ReadBuffer) uint8 {
	return uint8(buf.fixAt(0)[0])
}

func (t uint8Type) Read(buf *ReadBuffer, into any) (err error) {
	switch into := into.(type) {
	case interface{ VampFromUint8(uint8) error }:
		err = into.VampFromUint8(t.ReadUint8(buf))
	case *uint8:
		*into = t.ReadUint8(buf)
	case *any:
		*into = t.ReadUint8(buf)
	case *int:
		err = setIntU(into, t.ReadUint8(buf))
	case *uint:
		err = setUintU(into, t.ReadUint8(buf))
	case *int16:
		*into = int16(t.ReadUint8(buf))
	case *int32:
		*into = int32(t.ReadUint8(buf))
	case *int64:
		*into = int64(t.ReadUint8(buf))
	case *uint16:
		*into = uint16(t.ReadUint8(buf))
	case *uint32:
		*into = uint32(t.ReadUint8(buf))
	case *uint64:
		*into = uint64(t.ReadUint8(buf))
	default:
		err = cantRead("uint8", into)
	}
	return err
}

func (uint8Type) WriteUint8(buf *Buffer, i uint8) {
	mem := buf.fixAt(0)
	mem[0] = byte(i)
}

func (t uint8Type) Write(buf *Buffer, value any) (err error) {
	switch value := value.(type) {
	case interface{ VampToUint8() (uint8, error) }:
		v, err := value.VampToUint8()
		if err != nil {
			return err
		}
		t.WriteUint8(buf, v)
	case uint8:
		t.WriteUint8(buf, value)
	case *uint8:
		t.WriteUint8(buf, *value)
	default:
		err = wrInts("uint8", 0, math.MaxUint8, value, buf, t.WriteUint8)
	}
	return err
}

func (uint8Type) VampUnionAlt() (uint, any) { return uint8Alt, &Uint8 }

func (uint8Type) Compare(t Type, _ CompareOpts) (bool, int) {
	return sameAs[uint8Type](t)
}

func (uint8Type) Hash(h hash.Hash, _ CompareOpts) { h.Write([]byte{uint8Alt}) }

func (uint8Type) String() string { return "uint8" }

func (t uint8Type) PrintTo(w io.Writer) (int, error) {
	return io.WriteString(w, t.String())
}

type uint16Type struct{}

func (uint16Type) FixSize() uint { return 2 }

func (uint16Type) DynNo() uint { return 0 }

func (t uint16Type) Read(buf *ReadBuffer, into any) (err error) {
	switch into := into.(type) {
	case interface{ VampFromUint16(uint16) error }:
		err = into.VampFromUint16(t.ReadUint16(buf))
	case *uint16:
		*into = t.ReadUint16(buf)
	case *any:
		*into = t.ReadUint16(buf)
	case *int:
		err = setIntU(into, t.ReadUint16(buf))
	case *uint:
		err = setUintU(into, t.ReadUint16(buf))
	case *int32:
		*into = int32(t.ReadUint16(buf))
	case *int64:
		*into = int64(t.ReadUint16(buf))
	case *uint32:
		*into = uint32(t.ReadUint16(buf))
	case *uint64:
		*into = uint64(t.ReadUint16(buf))
	default:
		err = cantRead("uint16", into)
	}
	return err
}

func (t uint16Type) ReadUint16(buf *ReadBuffer) uint16 {
	mem := buf.fixAt(0)
	_ = mem[1] // bounds check hint to compiler
	return uint16(mem[0]) | uint16(mem[1])<<8
}

func (t uint16Type) Write(buf *Buffer, value any) (err error) {
	switch value := value.(type) {
	case interface{ VampToUint16() (uint16, error) }:
		v, err := value.VampToUint16()
		if err != nil {
			return err
		}
		t.WriteUint16(buf, v)
	case uint16:
		t.WriteUint16(buf, value)
	case *uint16:
		t.WriteUint16(buf, *value)
	case uint8:
		t.WriteUint16(buf, uint16(value))
	case *uint8:
		t.WriteUint16(buf, uint16(*value))
	default:
		err = wrInts("uint16", 0, math.MaxUint16, value, buf, t.WriteUint16)
	}
	return err
}

func (uint16Type) WriteUint16(buf *Buffer, value uint16) {
	mem := buf.fixAt(0)
	_ = mem[1] // bounds check hint to compiler
	mem[0] = byte(value)
	mem[1] = byte(value >> 8)
}

func (uint16Type) VampUnionAlt() (uint, any) { return uint16Alt, &Uint16 }

func (uint16Type) Compare(t Type, _ CompareOpts) (bool, int) {
	return sameAs[uint16Type](t)
}

func (uint16Type) Hash(h hash.Hash, _ CompareOpts) { h.Write([]byte{uint16Alt}) }

func (uint16Type) String() string { return "uint16" }

func (t uint16Type) PrintTo(w io.Writer) (int, error) {
	return io.WriteString(w, t.String())
}

type uint32Type struct{}

func (uint32Type) FixSize() uint { return 4 }

func (uint32Type) DynNo() uint { return 0 }

func (t uint32Type) Read(buf *ReadBuffer, into any) (err error) {
	switch into := into.(type) {
	case interface{ VampFromUint32(uint32) error }:
		err = into.VampFromUint32(t.ReadUint32(buf))
	case *uint32:
		*into = t.ReadUint32(buf)
	case *any:
		*into = t.ReadUint32(buf)
	case *int:
		err = setIntU(into, t.ReadUint32(buf))
	case *uint:
		err = setUintU(into, t.ReadUint32(buf))
	case *int64:
		*into = int64(t.ReadUint32(buf))
	case *uint64:
		*into = uint64(t.ReadUint32(buf))
	default:
		err = cantRead("uint32", into)
	}
	return err
}

func (t uint32Type) ReadUint32(buf *ReadBuffer) uint32 {
	return rdUint32(buf)
}

func (t uint32Type) Write(buf *Buffer, value any) (err error) {
	switch value := value.(type) {
	case interface{ VampToUint32() (uint32, error) }:
		v, err := value.VampToUint32()
		if err != nil {
			return err
		}
		t.WriteUint32(buf, v)
	case uint32:
		t.WriteUint32(buf, value)
	case *uint32:
		t.WriteUint32(buf, *value)
	case uint8:
		t.WriteUint32(buf, uint32(value))
	case *uint8:
		t.WriteUint32(buf, uint32(*value))
	case uint16:
		t.WriteUint32(buf, uint32(value))
	case *uint16:
		t.WriteUint32(buf, uint32(*value))
	default:
		err = wrInts("uint32", 0, math.MaxUint32, value, buf, t.WriteUint32)
	}
	return err
}

func (uint32Type) WriteUint32(buf *Buffer, value uint32) {
	wrUint32(buf, value)
}

func (uint32Type) VampUnionAlt() (uint, any) { return uint32Alt, &Uint32 }

func (uint32Type) Compare(t Type, _ CompareOpts) (bool, int) {
	return sameAs[uint32Type](t)
}

func (uint32Type) Hash(h hash.Hash, _ CompareOpts) { h.Write([]byte{uint32Alt}) }

func (uint32Type) String() string { return "uint32" }

func (t uint32Type) PrintTo(w io.Writer) (int, error) {
	return io.WriteString(w, t.String())
}

type uint64Type struct{}

func (uint64Type) FixSize() uint { return 8 }

func (uint64Type) DynNo() uint { return 0 }

func (t uint64Type) Read(buf *ReadBuffer, into any) (err error) {
	switch into := into.(type) {
	case interface{ VampFromUint64(uint64) error }:
		err = into.VampFromUint64(t.ReadUint64(buf))
	case *uint64:
		*into = t.ReadUint64(buf)
	case *any:
		*into = t.ReadUint64(buf)
	case *int:
		err = setIntU(into, t.ReadUint64(buf))
	case *uint:
		err = setUintU(into, t.ReadUint64(buf))
	default:
		err = cantRead("uint64", into)
	}
	return err
}

func (t uint64Type) ReadUint64(buf *ReadBuffer) uint64 {
	return rdUint64(buf)
}

func (t uint64Type) Write(buf *Buffer, value any) (err error) {
	switch value := value.(type) {
	case interface{ VampToUint64() (uint64, error) }:
		v, err := value.VampToUint64()
		if err != nil {
			return err
		}
		t.WriteUint64(buf, v)
	case uint64:
		t.WriteUint64(buf, value)
	case *uint64:
		t.WriteUint64(buf, *value)
	case uint8:
		t.WriteUint64(buf, uint64(value))
	case *uint8:
		t.WriteUint64(buf, uint64(*value))
	case uint16:
		t.WriteUint64(buf, uint64(value))
	case *uint16:
		t.WriteUint64(buf, uint64(*value))
	case uint32:
		t.WriteUint64(buf, uint64(value))
	case *uint32:
		t.WriteUint64(buf, uint64(*value))
	default:
		err = wrInts("uint64", 0, math.MaxUint64, value, buf, t.WriteUint64)
	}
	return err
}

func (uint64Type) WriteUint64(buf *Buffer, value uint64) {
	wrUint64(buf, value)
}

func (uint64Type) VampUnionAlt() (uint, any) { return uint64Alt, &Uint64 }

func (uint64Type) Compare(t Type, _ CompareOpts) (bool, int) {
	return sameAs[uint64Type](t)
}

func (uint64Type) Hash(h hash.Hash, _ CompareOpts) { h.Write([]byte{uint64Alt}) }

func (uint64Type) String() string { return "uint64" }

func (t uint64Type) PrintTo(w io.Writer) (int, error) {
	return io.WriteString(w, t.String())
}

type float32Type struct{}

func (float32Type) FixSize() uint { return 4 }

func (float32Type) DynNo() uint { return 0 }

func (float32Type) WriteFloat32(buf *Buffer, value float32) {
	wrUint32(buf, math.Float32bits(value))
}

func (t float32Type) Write(buf *Buffer, value any) error {
	switch value := value.(type) {
	case interface{ VampToFloat32() (float32, error) }:
		v, err := value.VampToFloat32()
		if err != nil {
			return err
		}
		t.WriteFloat32(buf, v)
	case float32:
		t.WriteFloat32(buf, value)
	case *float32:
		t.WriteFloat32(buf, *value)
	default:
		return cantWrite(value, "float32")
	}
	return nil
}

func (float32Type) ReadFloat32(buf *ReadBuffer) float32 {
	return math.Float32frombits(rdUint32(buf))
}

func (t float32Type) Read(buf *ReadBuffer, into any) (err error) {
	switch into := into.(type) {
	case interface{ VampFromFloat32(float32) error }:
		err = into.VampFromFloat32(t.ReadFloat32(buf))
	case *float32:
		*into = t.ReadFloat32(buf)
	case *any:
		*into = t.ReadFloat32(buf)
	case *float64:
		*into = float64(t.ReadFloat32(buf))
	default:
		err = cantRead("float64", into)
	}
	return err
}

func (float32Type) VampUnionAlt() (uint, any) { return float32Alt, &Float32 }

func (float32Type) Compare(t Type, _ CompareOpts) (bool, int) {
	return sameAs[float32Type](t)
}

func (float32Type) Hash(h hash.Hash, _ CompareOpts) { h.Write([]byte{float32Alt}) }

func (float32Type) String() string { return "float32" }

func (t float32Type) PrintTo(w io.Writer) (int, error) {
	return io.WriteString(w, t.String())
}

type float64Type struct{}

func (float64Type) FixSize() uint { return 8 }

func (float64Type) DynNo() uint { return 0 }

func (float64Type) WriteFloat64(buf *Buffer, value float64) {
	wrUint64(buf, math.Float64bits(value))
}

func (t float64Type) Write(buf *Buffer, value any) error {
	switch value := value.(type) {
	case interface{ VampToFloat64() (float64, error) }:
		v, err := value.VampToFloat64()
		if err != nil {
			return err
		}
		t.WriteFloat64(buf, v)
	case float64:
		t.WriteFloat64(buf, value)
	case *float64:
		t.WriteFloat64(buf, *value)
	case float32:
		t.WriteFloat64(buf, float64(value))
	case *float32:
		t.WriteFloat64(buf, float64(*value))
	default:
		return cantWrite(value, "float64")
	}
	return nil
}

func (float64Type) ReadFloat64(buf *ReadBuffer) float64 {
	return math.Float64frombits(rdUint64(buf))
}

func (t float64Type) Read(buf *ReadBuffer, into any) (err error) {
	switch into := into.(type) {
	case interface{ VampFromFloat64(float64) error }:
		err = into.VampFromFloat64(t.ReadFloat64(buf))
	case *float64:
		*into = t.ReadFloat64(buf)
	case *any:
		*into = t.ReadFloat64(buf)
	default:
		err = cantRead("float64", into)
	}
	return err
}

func (float64Type) VampUnionAlt() (uint, any) { return float64Alt, &Float64 }

func (float64Type) Compare(t Type, _ CompareOpts) (bool, int) {
	return sameAs[float64Type](t)
}

func (float64Type) Hash(h hash.Hash, _ CompareOpts) { h.Write([]byte{float64Alt}) }

func (float64Type) String() string { return "float64" }

func (t float64Type) PrintTo(w io.Writer) (int, error) {
	return io.WriteString(w, t.String())
}

func wrUint32(buf *Buffer, i uint32) {
	mem := buf.fixAt(0)
	_ = mem[3] // bounds check hint to compiler
	mem[0] = byte(i)
	mem[1] = byte(i >> 8)
	mem[2] = byte(i >> 16)
	mem[3] = byte(i >> 24)
}

func rdUint32(buf *ReadBuffer) uint32 {
	mem := buf.fixAt(0)
	_ = mem[3] // bounds check hint to compiler
	return uint32(mem[0]) | uint32(mem[1])<<8 | uint32(mem[2])<<16 | uint32(mem[3])<<24
}

func wrUint64(buf *Buffer, i uint64) {
	mem := buf.fixAt(0)
	_ = mem[7] // bounds check hint to compiler
	mem[0] = byte(i)
	mem[1] = byte(i >> 8)
	mem[2] = byte(i >> 16)
	mem[3] = byte(i >> 24)
	mem[4] = byte(i >> 32)
	mem[5] = byte(i >> 40)
	mem[6] = byte(i >> 48)
	mem[7] = byte(i >> 56)
}

func rdUint64(buf *ReadBuffer) uint64 {
	mem := buf.fixAt(0)
	_ = mem[7] // bounds check hint to compiler
	return uint64(mem[0]) | uint64(mem[1])<<8 | uint64(mem[2])<<16 | uint64(mem[3])<<24 |
		uint64(mem[4])<<32 | uint64(mem[5])<<40 | uint64(mem[6])<<48 | uint64(mem[7])<<56
}

func irangeError[I constraints.Integer](i I, ty string) error {
	return fmt.Errorf("%d exceeds %s range", i, ty)
}

func checkIntS(i int64) error {
	if i < math.MinInt || i > math.MaxInt {
		return irangeError(i, "int")
	}
	return nil
}

func checkIntU(i uint64) error {
	if i > math.MaxInt {
		return irangeError(i, "int")
	}
	return nil
}

func checkUintS(i int64) error {
	if i < 0 || uint(i) > math.MaxUint {
		return irangeError(i, "uint")
	}
	return nil
}

func checkUintU(i uint64) error {
	if i > math.MaxUint {
		return irangeError(i, "uint")
	}
	return nil
}

func setIntS[F constraints.Signed](to *int, from F) error {
	if err := checkIntS(int64(from)); err != nil {
		return err
	}
	*to = int(from)
	return nil
}

func setIntU[F constraints.Unsigned](to *int, from F) error {
	if err := checkIntU(uint64(from)); err != nil {
		return err
	}
	*to = int(from)
	return nil
}

func setUintS[F constraints.Signed](to *uint, from F) error {
	if err := checkUintS(int64(from)); err != nil {
		return err
	}
	*to = uint(from)
	return nil
}

func setUintU[F constraints.Unsigned](to *uint, from F) error {
	if err := checkUintU(uint64(from)); err != nil {
		return err
	}
	*to = uint(from)
	return nil
}

func wrInts[I constraints.Integer](n string, min, max I, v any, b *Buffer, w func(*Buffer, I)) error {
	switch v := v.(type) {
	case int:
		if I(v) < min || I(v) > max {
			return irangeError(v, n)
		}
		w(b, I(v))
	case uint:
		if I(v) > max {
			return irangeError(v, n)
		}
		w(b, I(v))
	case *int:
		if I(*v) < min || I(*v) > max {
			return irangeError(*v, n)
		}
		w(b, I(*v))
	case *uint:
		if I(*v) > max {
			return irangeError(*v, n)
		}
		w(b, I(*v))
	default:
		return cantWrite(v, "int16")
	}
	return nil
}
