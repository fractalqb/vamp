// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"fmt"
	"hash"
	"io"
	"math"
)

type CompareOpts int

const (
	// Compare takes Record field names into account
	FieldNames CompareOpts = (1 << iota)

	// Compare takes Union alt names into account
	AltNames
)

// Type is the interface of all Vampire types that are used to define the
// structure of marshaled data. Types can be created as [Array], [List],
// [Record] or [Union] form other types. Basic types are booleans, signed and
// unsigned integers with 8, 16, 32 or 64 bit and float with 32 or 64 bit. The
// special [Bytes] types are used to hold any binary data, especially UTF-8
// strings. Finally there is the [Any] type that can hold any Vampire value as a
// tuple of a value and its type.
type Type interface {
	FixSize() uint // fix size of the type in byte
	DynNo() uint   // number of dynamic elements of the type

	// Write the value to buffer if its structure matches Type
	Write(b *Buffer, value any) error

	// Read buffers content into argument if its structure matches Type
	Read(b *ReadBuffer, into any) error

	// Compare this type with type t. A type w is less than or equal to type r
	// if vampire data written by w can be ready with type r. Only if neither w
	// is less than or equal to r nor r is less than or equal to w is comparable
	// false. Otherwise, comparison is 0 for equal types, less than 0 if this
	// type is less than t or greater than 0 if this t is less than this type.
	Compare(t Type, opts CompareOpts) (comparable bool, comparison int)

	Hash(h hash.Hash, opts CompareOpts)

	fmt.Stringer
	PrintTo(w io.Writer) (int, error)
}

func Equal(s, t Type, opts CompareOpts) bool {
	ok, cmp := s.Compare(t, opts)
	return ok && cmp == 0
}

type namedType struct {
	name string
	Type
}

// Named attaches a name to a type. Type names are irrelevant for the encoded
// data end exist for documentation only. Passing a named type to the
// constructors of record or union types will assign the name to the respective
// record field or union variant.
func Named(n string, t Type) (Type, error) {
	if !ValidName(n) {
		return nil, fmt.Errorf("invalid name '%s'", n)
	}
	if nt, ok := t.(namedType); ok {
		return namedType{name: n, Type: nt.Type}, nil
	}
	return namedType{name: n, Type: t}, nil
}

func MustNamed(n string, t Type) Type {
	named, err := Named(n, t)
	if err != nil {
		panic(err)
	}
	return named
}

// TypeName returns the name attached to a type, if any. Otherwise TypeName
// returns the empty string.
func TypeName(t Type) string {
	if nt, ok := t.(namedType); ok {
		return nt.name
	}
	return ""
}

// Anonymized returns the unnamed type of t. If t is already unnamed it is
// returned.
func Anonymized(t Type) Type {
	for {
		if nt, ok := t.(namedType); ok {
			t = nt.Type
		} else {
			return t
		}
	}
}

// ValidName checks whether s is a valid name.
func ValidName(s string) bool {
	if s == "" {
		return false
	}
	for _, r := range s {
		if !isName(r) {
			return false
		}
	}
	return true
}

// SizeRanges are used in vamp to define the valid range for buffer offsets,
// string length and any sort of ranges of size in byte that is used in Vampire
// encoding.
type SizeRange uint8

//go:generate stringer -type SizeRange
const (
	// Size8 is a 8-bit size range with values 0…255
	Size8 SizeRange = iota

	// Size16 is a 16-bit size range with values 0…65535
	Size16

	// Size32 is a 32-bit size range with values 0…4294967295
	Size32

	sizeRangeNo // For range check on data input
)

func SizeRangeFromBits(n int) (SizeRange, error) {
	switch n {
	case 8:
		return Size8, nil
	case 16:
		return Size16, nil
	case 32:
		return Size32, nil
	}
	return Size32, fmt.Errorf("no SizeRange for %d bits", n)
}

func SizeRangeFromMax(max uint) (SizeRange, error) {
	for r := Size8; r < sizeRangeNo; r++ {
		if max <= r.Max() {
			return r, nil
		}
	}
	return Size32, fmt.Errorf("max=%d exceed all size ranges", max)
}

// Size returns the number of bytes used for the size range in Vampire encoding.
// SizeRange s is expected to be s.Valid()==true.
func (s SizeRange) Size() uint { return uint(1) << s }

var sizeRangeMax = [3]uint{math.MaxUint8, math.MaxUint16, math.MaxUint32}

// Max returns the maximum uint of SizeRange s. SizeRange s is expected to be
// s.Valid()==true.
func (s SizeRange) Max() uint { return sizeRangeMax[s] }

// Check checks if size is withing the SizeRange s. Only if size is not in s'
// range an error is returned.
func (s SizeRange) Check(size uint) error {
	if size > s.Max() {
		return fmt.Errorf("%d exceed Size%d range", size, 8*s.Size())
	}
	return nil
}

// Bounds check with: -gcflags="-d=ssa/check_bce/debug=1"
func (s SizeRange) writeFix(b *Buffer, at, size uint) error {
	switch s {
	case Size8:
		if size > math.MaxUint8 {
			return fmt.Errorf("size %d out of uint8 range", size)
		}
		b.fixAt(at)[0] = byte(size)
	case Size16:
		if size > math.MaxUint16 {
			return fmt.Errorf("size %d out of uint16 range", size)
		}
		mem := b.fixAt(at)
		_ = mem[1] // bounds check hint to compiler; see golang.org/issue/14808
		mem[0] = byte(size)
		mem[1] = byte(size >> 8)
	case Size32:
		if size > math.MaxUint32 {
			return fmt.Errorf("size %d out of uint32 range", size)
		}
		mem := b.fixAt(at)
		_ = mem[3] // bounds check hint to compiler; see golang.org/issue/14808
		mem[0] = byte(size)
		mem[1] = byte(size >> 8)
		mem[2] = byte(size >> 16)
		mem[3] = byte(size >> 24)
	default:
		// this is a private method! Must only be called on s.Valid()
		panic(invalidRange("size", s))
	}
	return nil
}

// TODO SizeRange remove error return
func (s SizeRange) readFix(b *ReadBuffer, at uint) uint {
	return s.read(b.fixAt(at))
}

func (s SizeRange) read(mem []byte) (size uint) {
	switch s {
	case Size8:
		size = uint(mem[0])
	case Size16:
		_ = mem[1] // bounds check hint to compiler; see golang.org/issue/14808
		size = uint(mem[0]) | uint(mem[1])<<8
	case Size32:
		_ = mem[3] // bounds check hint to compiler; see golang.org/issue/14808
		size = uint(mem[0]) | uint(mem[1])<<8 | uint(mem[2])<<16 | uint(mem[3])<<24
	default:
		// this is a private method! Must only be called on s.Valid()
		panic(invalidRange("size", s))
	}
	return size
}

// SizeRange can be encoded as Vampire Uint8.
func (s SizeRange) VampToUint8() (uint8, error) { return uint8(s), nil }

// SizeRange can be decoded from a Vampire Uint8.
func (s *SizeRange) VampFromUint8(i uint8) error {
	if i >= uint8(sizeRangeNo) {
		return invalidRange("size", SizeRange(i))
	}
	*s = SizeRange(i)
	return nil
}

// private method considers off to be valid
func sizeOf(t Type, off SizeRange) uint {
	return t.FixSize() + t.DynNo()*off.Size()
}

func cantWrite(val any, as string) error {
	return fmt.Errorf("vampire cannot write %T as %s", val, as)
}

func cantRead(ty string, into any) error {
	return fmt.Errorf("vampire cannot read %s into %T", ty, into)
}

func invalidRange(what string, r SizeRange) error {
	return fmt.Errorf("invalid %s range: %d", what, r)
}

func sameAs[T Type](t Type) (bool, int) {
	_, ok := t.(T)
	return ok, 0
}
