// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"fmt"
	"sync"
	"testing"
)

func ExampleList() {
	a := []string{"boo", "bar", "baz"}
	t := NewList(Size32, NewString(Size16))
	fmt.Println(t)
	buf := NewWriteBuffer(t, Size16, 0, nil)
	t.Write(buf, a)
	var out any
	t.Read(&buf.ReadBuffer, &out)
	fmt.Printf("%[1]T=%[1]v\n", out)
	// Output:
	// (Size32 "Size16")
	// []interface {}=[boo bar baz]
}

func TestListType_slice(t *testing.T) {
	ety := NewString(Size16)
	lty := NewList(Size16, ety)
	buf := NewWriteBuffer(lty, Size16, 0, nil)
	data := []string{"foo", "bar", "baz", "quux"}
	err := lty.Write(buf, data)
	if err != nil {
		t.Fatal(err)
	}
	var out []string
	err = lty.Read(&buf.ReadBuffer, &out)
	if err != nil {
		t.Fatal(err)
	}
	if len(data) != len(out) {
		t.Errorf("len(data)=%d != len(out)=%d", len(data), len(out))
	}
	if l, ok := lty.ListLen(&buf.ReadBuffer); !ok {
		t.Error("cannot read list len")
	} else if l != 4 {
		t.Errorf("wrong list len: %d", l)
	}
	for i, d := range data {
		if d != out[i] {
			t.Errorf("element %d: '%s' != '%s'", i, d, out[i])
		}
	}
}

func TestListType_Next(t *testing.T) {
	ety := NewString(Size16)
	lty := NewList(Size16, ety)
	buf := NewWriteBuffer(lty, Size16, 0, nil)
	data := []string{"foo", "bar", "baz", "quux"}
	err := lty.Write(buf, data)
	if err != nil {
		t.Fatal(err)
	}
	for i, want := range data {
		rev, ok := lty.Next(&buf.ReadBuffer)
		if !ok {
			t.Fatalf("no element %d in list", i)
		}
		out := ety.ReadString(&buf.ReadBuffer)
		buf.Unslice(rev)
		if out != want {
			t.Errorf("element %d: '%s' != '%s'", i, want, out)
		}
	}
	if _, ok := lty.Next(&buf.ReadBuffer); ok {
		t.Error("too many elements in list")
	}
}

func TestListType_arrayWriteRead(t *testing.T) {
	ety := NewString(Size16)
	lty := NewList(Size8, ety)
	buf := NewWriteBuffer(lty, Size16, 0, nil)
	data := [4]string{"foo", "bar", "baz", "quux"}
	err := lty.Write(buf, data)
	if err != nil {
		t.Fatal(err)
	}
	var out [4]string
	err = lty.Read(&buf.ReadBuffer, &out)
	if err != nil {
		t.Fatal(err)
	}
	for i, d := range data {
		if d != out[i] {
			t.Errorf("element %d: '%s' != '%s'", i, d, out[i])
		}
	}
}

func TestListType_chanWriteRead(t *testing.T) {
	strs := []string{"foo", "bar", "baz", "quux"}
	ety := NewString(Size16)
	lty := NewList(Size32, ety)
	buf := NewWriteBuffer(lty, Size16, 0, nil)
	data := make(chan string)
	go func() {
		for _, s := range strs {
			data <- s
		}
		close(data)
	}()
	err := lty.Write(buf, data)
	if err != nil {
		t.Fatal(err)
	}
	data = make(chan string)
	var out []any
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		for s := range data {
			out = append(out, s)
		}
		wg.Done()
	}()
	err = lty.Read(&buf.ReadBuffer, data)
	close(data)
	wg.Wait()
	if err != nil {
		t.Fatal(err)
	}
	for i, d := range strs {
		if d != out[i] {
			t.Errorf("element %d: '%s' != '%s'", i, d, out[i])
		}
	}
}
