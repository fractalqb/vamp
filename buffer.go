// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"bytes"
	"errors"
	"fmt"
)

// Header is the VMP header that can be written to buffers to mark the data to
// be Vampire encoded and to provide the offsetSize used for the data.
//
// The VMP header is "VMPx" where x encodes the vampire version and the offset
// size.s
type Header [4]byte

const (
	// Version of the implemented Vampire encoding schema
	Version = 0

	// Size of Vampire data header in byte
	HeaderSize = len(Header{})
)

// HeaderFor clapms s to valid range
func HeaderFor(s SizeRange) Header {
	vns := byte(Version<<2 | s&0x3)
	return [HeaderSize]byte{'V', 'M', 'P', vns}
}

func (h Header) Version() int { return int(h[3] >> 2) }

func (h Header) SizeRange() SizeRange {
	r := h[3] & 0x3
	if r == 3 {
		return Size32
	}
	return SizeRange(r)
}

func (h Header) String() string {
	return fmt.Sprintf("Vampire-v%d+%dbit", h.Version(), 8*h.SizeRange().Size())
}

// Bytes returns the header as byte slice with added capacity of cap byte.
func (h Header) Bytes(cap int) []byte {
	if cap <= 0 {
		return h[:]
	}
	return append(h[:], make([]byte, cap)...)
}

func ParseHeader(mem []byte) (h Header, pos int) {
	for len(mem) > HeaderSize {
		idx := bytes.Index(mem[pos:], []byte("VMP")) // TODO avoid alloc?!
		if idx < 0 {
			return h, -1
		}
		mem = mem[idx:]
		pos += idx
		if SizeRange(mem[3]&0x3) < sizeRangeNo {
			copy(h[:], mem)
			return h, pos
		}
		mem = mem[HeaderSize-1:]
		pos += HeaderSize - 1
	}
	return h, -1
}

// ReadBuffer adapts a Vampire encoded byte array to be used for read-only
// access. One has to use the specific Type that matches the encoded data to
// read the data from the buffer.
type ReadBuffer struct {
	mem   []byte
	offsz SizeRange
	start uint
}

// NewReadBuffer wraps buf for read access when the offsetSize used for the
// encoded data is known. The first byte of the data of Type t must be buf[0].
func NewReadBuffer(t Type, off SizeRange, buf []byte) *ReadBuffer {
	return &ReadBuffer{
		mem:   buf,
		offsz: off,
		start: 0,
	}
}

// NewReadBufferHeader wraps buf for read access when the offsetSize is provided
// by a VMP Header in buf. When search is true NewReadBufferHeader scans for the
// first valid VMP header and uses it's offsetSize. Otherwise the VMP header
// must start at buf[0].
func NewReadBufferHeader(t Type, buf []byte, search bool) (*ReadBuffer, error) {
	hdr, pos := ParseHeader(buf)
	if pos < 0 {
		return nil, errors.New("missing VMP header")
	}
	if pos > 0 && !search {
		return nil, errors.New("buffer does not start with VMP header")
	}
	if hdr.Version() != Version {
		return nil, fmt.Errorf(
			"incompatible file version Vampire file %d, need %d",
			hdr.Version(),
			Version,
		)
	}
	return NewReadBuffer(t, hdr.SizeRange(), buf[pos+HeaderSize:]), nil
}

func (b *ReadBuffer) Bytes() []byte { return b.mem[b.start:] }

func (b *ReadBuffer) AllBytes() []byte { return b.mem }

func (b *ReadBuffer) Offset() SizeRange { return b.offsz }

func (b *ReadBuffer) sliceFix(off uint) (revert uint) {
	revert = b.start
	b.start += off
	return revert
}

func (b *ReadBuffer) Unslice(revert uint) {
	b.start = revert
}

func (b *ReadBuffer) readOff(at uint) uint { return b.offsz.readFix(b, at) }

func (b *ReadBuffer) fixAt(off uint) []byte { return b.mem[b.start+off:] }

type Buffer struct {
	ReadBuffer
}

func NewWriteBuffer(t Type, off SizeRange, at int, buf []byte) *Buffer {
	if at < 0 {
		at = 0
	}
	res := &Buffer{ReadBuffer{offsz: off, start: uint(at)}}
	fixSz := int(sizeOf(t, off)) + at
	if cap(buf) < fixSz {
		res.mem = make([]byte, fixSz)
		copy(res.mem, buf)
	} else {
		res.mem = buf[:fixSz]
	}
	return res
}

func (b *Buffer) Reset(t Type, off SizeRange, prefix int) {
	b.offsz = off
	b.start = uint(prefix)
	sz := sizeOf(t, off) + b.start
	if uint(cap(b.mem)) < sz {
		b.mem = make([]byte, sz)
	}
	b.mem = b.mem[:sz]
}

func (b *Buffer) appendSlice(size uint) (revert uint) {
	revert = b.start
	b.start = uint(len(b.mem))
	sz := b.start + size
	b.mem = makeLen(b.mem, sz)
	return revert
}

func (b *Buffer) writeOff(at uint) error {
	off := uint(len(b.mem)) - b.start - at - b.offsz.Size()
	return b.offsz.writeFix(b, at, off)
}

func (b *Buffer) append(data []byte) { b.mem = append(b.mem, data...) }

func (b *Buffer) appendString(s string) { b.mem = append(b.mem, s...) }

func makeLen(b []byte, l uint) []byte {
	if uint(cap(b)) < l {
		return append(b, make([]byte, l-uint(len(b)))...)
	}
	return b[:l]
}
