// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"fmt"
	"testing"
)

func TestTypeIO_basic(t *testing.T) {
	var buf Buffer
	types := []Type{
		Bool,
		Int8, Int16, Int32, Int64,
		Uint8, Uint16, Uint32, Uint64,
		Float32, Float64,
	}
	for _, typ := range types {
		t.Run(typ.(fmt.Stringer).String(), func(t *testing.T) {
			ResetTypeBuffer(&buf, Size16, 0)
			err := WriteType(&buf, typ)
			if err != nil {
				t.Fatal(err)
			}
			rty, err := ReadType(&buf.ReadBuffer)
			if err != nil {
				t.Fatal(err)
			}
			if rty != typ {
				t.Errorf("wrote %+v, read %+v", typ, rty)
			}
		})
	}
}

func TestBytesType_typeIO(t *testing.T) {
	var buf Buffer
	for sz := Size8; sz < sizeRangeNo; sz++ {
		typ := NewBytes(sz, IOBytesMode)
		t.Run(typ.String(), func(t *testing.T) {
			ResetTypeBuffer(&buf, Size16, 0)
			err := WriteType(&buf, typ)
			if err != nil {
				t.Fatal(err)
			}
			rty, err := ReadType(&buf.ReadBuffer)
			if err != nil {
				t.Fatal(err)
			}
			if *rty.(*Bytes) != typ {
				t.Errorf("wrote %+v, read %+v", typ, rty)
			}
		})
	}
}

func TestArrayType_typeIO(t *testing.T) {
	arr := NewArray(
		Size16,
		NewBytes(Size16, ReadRaw),
	)
	buf := NewTypeWriteBuffer(Size16, 0, nil)
	err := WriteType(buf, arr)
	if err != nil {
		t.Fatal(err)
	}
	out, err := ReadType(&buf.ReadBuffer)
	if err != nil {
		t.Fatal(err)
	}
	if !Equal(arr, out, AltNames) {
		t.Error(arr, "!=", out)
	}
}

func TestRecordType_typeIO(t *testing.T) {
	rec := NewRecord(
		MustNamed("id", Int32),
		MustNamed("name", NewString(Size16)),
		MustNamed("height", Float64),
	)
	buf := NewTypeWriteBuffer(Size16, 0, nil)
	err := WriteType(buf, rec)
	if err != nil {
		t.Fatal(err)
	}
	out, err := ReadType(&buf.ReadBuffer)
	if err != nil {
		t.Fatal(err)
	}
	if !Equal(rec, out, AltNames) {
		t.Error(rec, "!=", out)
	}
}

func TestListType_typeIO(t *testing.T) {
	ls := NewList(Size16, NewBytes(Size16, ReadRaw))
	buf := NewTypeWriteBuffer(Size16, 0, nil)
	err := WriteType(buf, ls)
	if err != nil {
		t.Fatal(err)
	}
	out, err := ReadType(&buf.ReadBuffer)
	if err != nil {
		t.Fatal(err)
	}
	if !Equal(ls, out, AltNames) {
		t.Error(ls, "!=", out)
	}
}

func TestUnionType_typeIO(t *testing.T) {
	ut := NewUnion(
		Size32,
		Bool,
		Float32,
		NewBytes(Size32, ReadRaw),
	)
	buf := NewTypeWriteBuffer(Size16, 0, nil)
	err := WriteType(buf, ut)
	if err != nil {
		t.Fatal(err)
	}
	out, err := ReadType(&buf.ReadBuffer)
	if err != nil {
		t.Fatal(err)
	}
	if !Equal(ut, out, AltNames) {
		t.Error(ut, "!=", out)
	}
}

func TestAnyType_typeIO(t *testing.T) {
	buf := NewTypeWriteBuffer(Size8, 0, nil)
	err := WriteType(buf, Any)
	if err != nil {
		t.Fatal(err)
	}
	out, err := ReadType(&buf.ReadBuffer)
	if err != nil {
		t.Fatal(err)
	}
	if !Equal(out, Any, AltNames) {
		t.Error(Any, "!=", out)
	}
}
