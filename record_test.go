// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"fmt"
	"testing"

	"git.fractalqb.de/fractalqb/testerr"
)

type testRecord struct {
	ID   int32
	Name string
	Flag bool
}

func ExampleRecord() {
	r := struct {
		Name string
		Age  uint8
	}{
		Name: "John Doe",
		Age:  44,
	}
	t, _, _ := ReflectType(r)
	fmt.Println(t)
	buf := NewWriteBuffer(t, Size16, 0, nil)
	t.Write(buf, r)
	var out any
	t.Read(&buf.ReadBuffer, &out)
	fmt.Printf("%[1]T=%[1]v\n", out)
	// Output:
	// {Name:"Size16" Age:uint8}
	// map[string]interface {}=map[Age:44 Name:John Doe]
}

func TestRecordType_rwFields(t *testing.T) {
	data := testRecord{ID: 4711, Name: "foo", Flag: true}
	rt := NewRecord(Int32, NewString(Size16), Bool)
	buf := NewWriteBuffer(rt, Size16, 0, nil)
	err := rt.WriteFields(buf, data.ID, data.Name, data.Flag)
	if err != nil {
		t.Fatal(err)
	}
	data = testRecord{}
	err = rt.ReadFields(&buf.ReadBuffer, &data.ID, &data.Name, &data.Flag)
	if err != nil {
		t.Fatal(err)
	}
	if !data.Flag {
		t.Error("bool field is false")
	}
	if data.Name != "foo" {
		t.Errorf("wrong string value '%s'", data.Name)
	}
	if data.ID != 4711 {
		t.Errorf("wrong int32 value %d", data.ID)
	}
	t.Logf("encoded record size: %d", len(buf.Bytes()))
}

func TestRecordType_Read(t *testing.T) {
	out := testRecord{ID: 4711, Name: "foo", Flag: true}
	rt := NewRecord(
		MustNamed("i", Int32),
		MustNamed("n", NewString(Size16)),
		MustNamed("f", Bool),
	)
	buf := NewWriteBuffer(rt, Size16, 0, nil)
	err := rt.Write(buf, out)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("encoded record size: %d", len(buf.Bytes()))
	t.Run("any", func(t *testing.T) {
		var out any
		err = rt.Read(&buf.ReadBuffer, &out)
		if err != nil {
			t.Fatal(err)
		}
		ov, ok := out.(map[string]any)
		if !ok {
			t.Fatalf("out type is %T", out)
		}
		if len(ov) != 3 {
			t.Errorf("out has length %d", len(ov))
		}
		if v := ov["i"]; v != int32(4711) {
			t.Errorf("out[0] is %[1]T=%+[1]v", v)
		}
		if v := ov["n"]; v != "foo" {
			t.Errorf("out[1] is %[1]T=%+[1]v", v)
		}
		if v := ov["f"]; v != true {
			t.Errorf("out[2] is %[1]T=%+[1]v", v)
		}
	})
	t.Run("struct", func(t *testing.T) {
		var out testRecord
		err = rt.Read(&buf.ReadBuffer, &out)
		if err != nil {
			t.Fatal(err)
		}
		if !out.Flag {
			t.Error("bool field is false")
		}
		if out.Name != "foo" {
			t.Errorf("wrong string value '%s'", out.Name)
		}
		if out.ID != 4711 {
			t.Errorf("wrong int32 value %d", out.ID)
		}
	})
	t.Run("map", func(t *testing.T) {
		out := make(map[string]any)
		err = rt.Read(&buf.ReadBuffer, out)
		if err != nil {
			t.Fatal(err)
		}
		want := map[string]any{
			"i": int32(4711),
			"n": "foo",
			"f": true,
		}
		if l := len(want); l != len(out) {
			t.Errorf("len(out) = %d", l)
		}
		for k, v := range want {
			if w := out[k]; w != v {
				t.Errorf("field '%s'=%[2]v:%[2]T; want %[3]v:%[3]T", k, w, v)
			}
		}
	})
}

func TestRecordType_randomRead(t *testing.T) {
	data := testRecord{ID: 4711, Name: "foo", Flag: true}
	rt := NewRecord(Int32, NewString(Size16), Bool)
	buf := NewWriteBuffer(rt, Size16, 0, nil)
	err := rt.WriteFields(buf, data.ID, data.Name, data.Flag)
	if err != nil {
		t.Fatal(err)
	}
	for i := int(rt.NumField()) - 1; i >= 0; i-- {
		rev := rt.Field(&buf.ReadBuffer, uint(i))
		var v any
		err := rt.FieldType(uint(i)).Read(&buf.ReadBuffer, &v)
		buf.Unslice(rev)
		if err != nil {
			t.Error(err)
		}
		switch i {
		case 0:
			if v != data.ID {
				t.Errorf("wrong field ID: %[1]T=%+[1]v", v)
			}
		case 1:
			if v != data.Name {
				t.Errorf("wrong field Name: %[1]T=%+[1]v", v)
			}
		case 2:
			if v != data.Flag {
				t.Errorf("wrong field Flag: %[1]T=%+[1]v", v)
			}
		}
	}
}

func FuzzRecordType(f *testing.F) {
	testcases := []testRecord{
		{},
		{4711, "Mustermann", true},
		{rune('K'), "", false},
	}
	for _, tc := range testcases {
		f.Add(tc.ID, tc.Name, tc.Flag)
	}
	typ, _, err := ReflectType(testcases[0])
	testerr.Shall(err).BeNil(f)
	buf := NewWriteBuffer(typ, Size16, 0, nil)
	f.Fuzz(func(t *testing.T, id int32, name string, flag bool) {
		buf.Reset(typ, buf.Offset(), 0)
		in := testRecord{id, name, flag}
		if err := typ.Write(buf, in); err != nil {
			t.Fatal(err)
		}
		var out testRecord
		if err := typ.Read(&buf.ReadBuffer, &out); err != nil {
			t.Fatal(err)
		}
		if out.ID != in.ID {
			t.Errorf("ID %d != %d", out.ID, in.ID)
		}
		if out.Name != in.Name {
			t.Errorf("Name '%s' != '%s'", out.Name, in.Name)
		}
		if out.Flag != in.Flag {
			t.Errorf("Flag %t != %t", out.Flag, in.Flag)
		}
	})
}
