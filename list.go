// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"fmt"
	"hash"
	"io"
	"reflect"
	"strings"

	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/vamp/internal"
)

type ListMarshaler interface {
	VampListIter() ListIterator
}

type ListIterator interface {
	VampListNext() bool
	VampListGet() any
}

type List struct {
	lenRange SizeRange
	elmType  Type
}

type ListUnmarshaler interface {
	VampListReader() (any, error)
	VampListAppend(any) error
}

func NewList(length SizeRange, elem Type) *List {
	return &List{
		lenRange: length,
		elmType:  Anonymized(elem),
	}
}

func (t *List) LenRange() SizeRange { return t.lenRange }

func (t *List) Elem() Type { return t.elmType }

func (t *List) FixSize() uint { return t.lenRange.Size() }

func (t *List) DynNo() uint { return 1 }

func (t *List) ListLen(buf *ReadBuffer) (len uint, ok bool) {
	len = t.lenRange.readFix(buf, buf.Offset().Size())
	if len > 0 {
		return len, true
	}
	off := buf.readOff(0)
	return 0, (off == 0)
}

// TODO Test ListType.Next()
func (t *List) Next(buf *ReadBuffer) (revert uint, ok bool) {
	off := buf.readOff(0)
	if off == 0 {
		return buf.start, false
	}
	offsz := buf.Offset().Size()
	buf.sliceFix(offsz + off)
	revert = buf.start
	buf.sliceFix(offsz)
	return revert, true
}

func (t *List) Write(buf *Buffer, val any) error {
	if m, ok := val.(ListMarshaler); ok {
		return t.WriteList(buf, m)
	}
	rval := reflect.ValueOf(val)
	switch rval.Kind() {
	case reflect.Array, reflect.Slice:
		ls := listSlice{val: rval, it: -1}
		return t.WriteList(buf, &ls)
	case reflect.Chan:
		ls := listChan{val: rval}
		return t.WriteList(buf, &ls)
	default:
		cantWrite(val, "list")
	}
	return nil
}

func (t *List) WriteList(buf *Buffer, ls ListMarshaler) error {
	var lslen uint
	rev := buf.start
	defer func() {
		buf.Unslice(rev)
		if max := t.lenRange.Max(); lslen <= max {
			t.lenRange.writeFix(buf, buf.Offset().Size(), lslen)
		}
	}()
	offSz := buf.Offset().Size()
	zeroOff := func() {
		mem := buf.fixAt(0)
		clear(mem[:offSz])
	}
	zeroOff()
	t.lenRange.writeFix(buf, offSz, 0)
	iter := ls.VampListIter()
	for iter.VampListNext() {
		err := buf.writeOff(0)
		if err != nil {
			return errAt(t, lslen, err)
		}
		buf.appendSlice(offSz + sizeOf(t.elmType, buf.Offset()))
		erev := buf.start
		zeroOff()
		buf.sliceFix(offSz)
		if err = t.elmType.Write(buf, iter.VampListGet()); err != nil {
			return errAt(t, lslen, err)
		}
		buf.Unslice(erev)
		lslen++
	}
	return nil
}

func (t *List) Read(buf *ReadBuffer, into any) error {
	switch into := into.(type) {
	case ListUnmarshaler:
		return t.ReadList(buf, into)
	case *any:
		var tmp []any
		val := reflect.Indirect(reflect.ValueOf(&tmp))
		ls := listSlice{val: val}
		if err := t.ReadList(buf, &ls); err != nil {
			return err
		}
		*into = tmp
		return nil
	}
	val := reflect.Indirect(reflect.ValueOf(into))
	switch val.Kind() {
	case reflect.Slice:
		ls := listSlice{val: val}
		return t.ReadList(buf, &ls)
	case reflect.Array:
		ls := listArrayRead{val: val}
		return t.ReadList(buf, &ls)
	case reflect.Chan:
		ls := listChan{val: val}
		return t.ReadList(buf, &ls)
	}
	return cantRead("list", into)
}

func (t *List) ReadList(buf *ReadBuffer, into ListUnmarshaler) error {
	rev := buf.start
	defer buf.Unslice(rev)
	off := buf.readOff(0)
	off += buf.Offset().Size()
	count := uint(0)
	for {
		buf.sliceFix(off)
		erev := buf.sliceFix(buf.Offset().Size())
		rd, err := into.VampListReader()
		if err != nil {
			return errAt(t, count, err)
		}
		if err = t.elmType.Read(buf, rd); err != nil {
			return errAt(t, count, err)
		}
		buf.Unslice(erev)
		off = buf.readOff(0)
		if err = into.VampListAppend(rd); err != nil {
			return errAt(t, count, err)
		}
		if off == 0 {
			break
		}
		off += buf.Offset().Size()
		count++
	}
	return nil
}

func (l *List) Compare(t Type, opts CompareOpts) (bool, int) {
	k, ok := t.(*List)
	if !ok {
		return false, 0
	}
	if l.lenRange != k.lenRange {
		return false, 0
	}
	return l.elmType.Compare(k.elmType, opts)
}

func (a *List) Hash(h hash.Hash, opts CompareOpts) {
	h.Write([]byte{listAlt, byte(a.lenRange)})
	a.elmType.Hash(h, opts)
}

func (t *List) String() string {
	var sb strings.Builder
	sb.WriteByte('(')
	sb.WriteString(t.lenRange.String())
	sb.WriteByte(' ')
	sb.WriteString(t.elmType.String())
	sb.WriteByte(')')
	return sb.String()
}

func (t *List) PrintTo(w io.Writer) (n int, err error) {
	iw, ok := internal.WIndent(w)
	if ok {
		defer must.RecoverAs(&err)
	}
	n += must.Ret(fmt.Fprintf(iw, "(%s ", t.lenRange.String()))
	n += must.Ret(t.elmType.PrintTo(iw))
	n += must.Ret(io.WriteString(iw, ")"))
	if ok {
		n += must.Ret(fmt.Fprintln(iw))
	}
	return n, nil
}

type listSlice struct {
	val reflect.Value
	it  int
}

func (l *listSlice) VampListIter() ListIterator { return l }

func (l *listSlice) VampListNext() bool {
	if i := l.it + 1; i < l.val.Len() {
		l.it = i
		return true
	}
	return false
}

func (l *listSlice) VampListGet() any { return l.val.Index(l.it).Interface() }

func (l *listSlice) VampListReader() (any, error) {
	e := reflect.New(l.val.Type().Elem())
	l.val.Set(reflect.Append(l.val, e.Elem()))
	e = l.val.Index(l.val.Len() - 1).Addr()
	return e.Interface(), nil
}

func (*listSlice) VampListAppend(any) error { return nil }

type listArrayRead struct {
	val reflect.Value
	it  int
}

func (l *listArrayRead) VampListReader() (any, error) {
	if l.it >= l.val.Len() {
		return nil, fmt.Errorf("index %d out of array bounds [0:%d]", l.it, l.val.Len())
	}
	e := reflect.New(l.val.Type().Elem())
	l.val.Index(l.it).Set(e.Elem())
	e = l.val.Index(l.it).Addr()
	l.it++
	return e.Interface(), nil
}

func (*listArrayRead) VampListAppend(any) error { return nil }

type listChan struct {
	val  reflect.Value
	next any
}

func (l *listChan) VampListIter() ListIterator { return l }

func (l *listChan) VampListNext() bool {
	tmp, ok := l.val.Recv()
	if ok {
		l.next = tmp.Interface()
	} else {
		l.next = nil
	}
	return ok
}

func (l *listChan) VampListGet() any { return l.next }

func (l *listChan) VampListReader() (any, error) {
	e := reflect.New(l.val.Type().Elem())
	return e.Interface(), nil
}

func (l *listChan) VampListAppend(rd any) error {
	val := reflect.ValueOf(rd)
	l.val.Send(val.Elem())
	return nil
}

func (t *List) VampRecordWrite(rt *Record, buf *Buffer) error {
	return rt.WriteFields(buf, t.lenRange, t.elmType)
}

func (t *List) VampRecordRead(rt *Record, buf *ReadBuffer) error {
	err := rt.ReadFields(buf, &t.lenRange, &t.elmType)
	if err != nil {
		return err
	}
	return nil
}

func (t *List) VampUnionAlt() (uint, any) { return listAlt, t }
