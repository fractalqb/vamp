// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"log"
	"os"

	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/vamp"
)

const dataFile = "persons.vmp"

type Person struct {
	ID    int
	Name  string   `vamp:",size=8"`
	Nicks []string `vamp:",size=8:8"`
}

var vampPerson = vamp.MustUseStruct(Person{})

func main() {
	log.Println(vampPerson)

	// For vamp.List one does not need to know the numer of elements in advance.
	personList := vamp.NewList(vamp.Size16, vampPerson)
	must.Do(vamp.WriteTypeFile("persons.vmpt", vamp.Size8, personList))

	writeStream(personList)
	readStream(personList)
}

func writeStream(listType *vamp.List) {
	file := must.Ret(os.Create(dataFile))
	defer file.Close()
	ls := must.Ret(listType.WriteStream(file,
		vamp.Size8,
		vamp.HeaderFor(vamp.Size8).Bytes(0),
	))

	must.Do(ls.Write(Person{
		ID:    4711,
		Name:  "John Doe",
		Nicks: []string{"Dowie", "Jonny"},
	}))
	must.Do(ls.Write(Person{
		ID:    1147,
		Name:  "Max Mustermann",
		Nicks: []string{"Mussi", "Mäxchen"},
	}))

	ls.Flush()
	file.Close()
}

func readStream(listType *vamp.List) {
	file := must.Ret(os.Open(dataFile))
	defer file.Close()
	var header vamp.Header
	must.Ret(file.Read(header[:]))
	ls := listType.ReadStream(file, header.SizeRange(), nil)
	for ls.Next() {
		var p Person
		ls.Read(&p)
		fmt.Println("Read:", p)
	}
	must.Do(ls.Err())
}
