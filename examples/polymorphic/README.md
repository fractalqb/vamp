Running this example, e.g. with

```
$ go run main.go
```
will create two files:

1. `account.vmpt`: This is the account type written as Vampire file

2. `account.vmp`: This is an example account record written as Vampire file.

You can run the `vamp` command (see `../../cmd/vamp`):

```
$ vamp -i "  " -t account.vmpt account.vmp
```
to dump the example record as JSON:

``` JSON
[
  "John Doe",
  [
    "j.d@acme.dummy.none",
    [
      "49",
      "55555",
      "0786"
    ]
  ]
]
```