// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

/*
Package polymorphic demonstartes how to use unions.
*/
package main

import (
	"encoding/json"
	"fmt"
	"os"

	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/vamp"
)

type Contact interface {
	vamp.UnionMarshaler
}

// ContactVamp is polymorphic
var ContactVamp = vamp.NewUnion(vamp.Size8,
	PhoneVamp,
	EMailVamp,
).SetAltReader(func(alt uint, into any) (any, error) {
	in, ok := into.(*Contact)
	if !ok {
		return nil, fmt.Errorf("cannot read contact into %T", into)
	}
	switch alt {
	case ContactPhone:
		p := new(Phone)
		*in = p
		return p, nil
	case ContactEMail:
		m := new(EMail)
		*in = m
		return m, nil
	}
	return nil, fmt.Errorf("cannot read alt %d into %T", alt, into)
})

type Phone struct {
	CC, NDC, SN string
}

func (p Phone) String() string {
	return fmt.Sprintf("+%s %s %s", p.CC, p.NDC, p.SN)
}

func (p *Phone) VampUnionAlt() (uint, any) { return ContactPhone, p }

var PhoneVamp = vamp.DefaultReflect().
	StringSize(vamp.Size8).
	MustUseStruct(Phone{})

type EMail string

func (m EMail) VampToString() (string, error) { return string(m), nil }

func (m *EMail) VampFromString(s string) error {
	*m = EMail(s)
	return nil
}

func (m EMail) VampUnionAlt() (uint, any) { return ContactEMail, m }

var EMailVamp = vamp.NewString(vamp.Size8)

type Account struct {
	Name     string
	Contacts []Contact
}

const (
	ContactPhone uint = iota
	ContactEMail
)

var AccountVamp = vamp.NewRecord(
	vamp.NewString(vamp.Size8),
	vamp.NewArray(vamp.Size8, ContactVamp),
)

func main() {
	fmt.Println(AccountVamp)

	must.Do(vamp.WriteTypeFile("account.vmpt", vamp.Size16, AccountVamp))

	buf := vamp.NewWriteBuffer(
		AccountVamp,
		vamp.Size16,
		vamp.HeaderSize,
		vamp.HeaderFor(vamp.Size16).Bytes(0),
	)
	must.Do(AccountVamp.Write(buf, Account{
		Name: "John Doe",
		Contacts: []Contact{
			EMail("j.d@acme.dummy.none"),
			&Phone{CC: "49", NDC: "55555", SN: "0786"},
		},
	}))
	var out Account
	must.Do(AccountVamp.Read(&buf.ReadBuffer, &out))
	json.NewEncoder(os.Stdout).Encode(out)
	must.Do(os.WriteFile("account.vmp", buf.AllBytes(), 0666))
}
