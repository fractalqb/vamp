Running this example, e.g. with

```
$ go run main.go
```
will create one file `person.vmp`. This is an example person record written as
Vampire file.

You can run the `vamp` command (see `../../cmd/vamp`):

```
$ vamp -i "  " person.vmp
```
to dump the example record as JSON:

``` JSON
{
  "ID": 4711,
  "Name": "John Doe",
  "Nicks": [
    "Dowie",
    "Jonny"
  ]
}
```
