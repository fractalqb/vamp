// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

/*
Package selfcontained demonstrates how to use the any type to create a
self-contained vmp file that can be read without an external type definition.
*/
package main

import (
	"log"
	"os"

	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/vamp"
)

type Person struct {
	ID    int
	Name  string   `vamp:",size=8"`
	Nicks []string `vamp:",size=8:8"`
}

var vampPerson = vamp.MustUseStruct(Person{})

func main() {
	log.Println(vampPerson)
	buf := vamp.NewWriteBuffer(
		vamp.Any,
		vamp.Size16,
		vamp.HeaderSize,
		vamp.HeaderFor(vamp.Size16).Bytes(0),
	)
	must.Do(vamp.Any.WriteAny(buf, vampPerson, Person{
		ID:    4711,
		Name:  "John Doe",
		Nicks: []string{"Dowie", "Jonny"},
	}))
	must.Do(os.WriteFile("person.vmp", buf.AllBytes(), 0666))
}
