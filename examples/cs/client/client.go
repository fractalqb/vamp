// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"flag"
	"fmt"
	"net"
	"time"

	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/vamp"
	"git.fractalqb.de/fractalqb/vamp/examples/cs/protocol"
)

var (
	addr string = "localhost:8000"

	vampReqList = vamp.NewList(vamp.Size8, protocol.VampRequest)
	vampRspList = vamp.NewList(vamp.Size8, protocol.VampResponse)
)

func main() {
	flag.StringVar(&addr, "addr", addr, "Listen address")
	flag.Parse()
	conn := must.Ret(net.Dial("tcp", addr))
	reqs := must.Ret(vampReqList.WriteStream(conn, protocol.StreamOffSize, nil))
	rsps := vampRspList.ReadStream(conn, protocol.StreamOffSize, nil)

	for i := 0; i < 2; i++ {
		must.Do(reqs.WriteWithNext(&protocol.HelloRequest{
			RequestHeader: protocol.RequestHeader{
				Token:   "4711",
				MsgTime: time.Now(),
			},
			ReqProto: protocol.ReqVersion,
			RspProto: protocol.RspVersion,
		}))
		var rsp any
		rsps.Next()
		must.Do(rsps.Read(&rsp))
		switch rsp := rsp.(type) {
		case *protocol.ResponseCode:
			fmt.Printf("Empyt response, code=%[1]d %[1]s\n", *rsp)
		default:
			fmt.Printf("Hello response: %[1]T=%+[1]v\n", rsp)
		}
	}
}
