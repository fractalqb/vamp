// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package protocol

import (
	"crypto/sha1"
	"fmt"
	"time"

	"git.fractalqb.de/fractalqb/vamp"
)

const StreamOffSize = vamp.Size32

var (
	ReqVersion []byte
	RspVersion []byte
)

func init() {
	const cmprOpts = vamp.FieldNames | vamp.AltNames
	h := sha1.New()
	VampRequest.Hash(h, cmprOpts)
	ReqVersion = h.Sum(nil)
	h.Reset()
	VampResponse.Hash(h, cmprOpts)
	RspVersion = h.Sum(nil)
}

type RequestHeader struct {
	Token   string `vamp:"size=8"`
	MsgTime time.Time
}

type HelloRequest struct {
	RequestHeader
	ReqProto []byte `vamp:"size=8"`
	RspProto []byte `vamp:"size=8"`
}

func (rq *HelloRequest) VampUnionAlt() (uint, any) { return ReqIDHello, rq }

const (
	ReqIDHello uint = iota
)

var VampRequest = vamp.NewUnion(vamp.Size8,
	vamp.MustNamed("hello", vamp.MustUseStruct(HelloRequest{})),
).SetAltReader(func(alt uint, into any) (any, error) {
	i := into.(*any) // Assume that server will read into any
	switch alt {
	case ReqIDHello:
		*i = new(HelloRequest)
		return *i, nil
	}
	return nil, fmt.Errorf("unknown request id %d", alt)
})

//go:generate stringer -type ResponseCode
type ResponseCode uint16

func (rc *ResponseCode) VampFromUint16(i uint16) error {
	*rc = ResponseCode(i) // TODO range check
	return nil
}

func (rp ResponseCode) VampUnionAlt() (uint, any) { return RspIDEmpty, uint16(rp) }

const (
	RspOK ResponseCode = iota
	RspProtoMismatch
)

const (
	RspIDEmpty uint = iota
)

var VampResponse = vamp.NewUnion(vamp.Size8,
	vamp.MustNamed("empty", vamp.Uint16),
).SetAltReader(func(alt uint, into any) (any, error) {
	i := into.(*any) // Assume that client will read into any
	switch alt {
	case RspIDEmpty:
		*i = new(ResponseCode)
		return *i, nil
	}
	return nil, fmt.Errorf("unknown response id %d", alt)
})
