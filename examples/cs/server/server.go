// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"encoding/hex"
	"flag"
	"log"
	"net"
	"reflect"

	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/vamp"
	"git.fractalqb.de/fractalqb/vamp/examples/cs/protocol"
)

var (
	addr = "localhost:8000"

	vampReqList = vamp.NewList(vamp.Size8, protocol.VampRequest)
	vampRspList = vamp.NewList(vamp.Size8, protocol.VampResponse)
)

func main() {
	flag.StringVar(&addr, "addr", addr, "Listen address")
	flag.Parse()
	ln := must.Ret(net.Listen("tcp", addr))
	log.Println("Server listening at:", addr)
	for {
		conn := must.Ret(ln.Accept())
		log.Println("Connection from ", conn.RemoteAddr())
		reqs := vampReqList.ReadStream(conn, protocol.StreamOffSize, nil)
		rsps := must.Ret(vampRspList.WriteStream(conn, protocol.StreamOffSize, nil))
		for reqs.Next() {
			var req any
			must.Do(reqs.Read(&req))
			switch req := req.(type) {
			case *protocol.HelloRequest:
				handleHello(rsps, req)
			default:
				log.Printf("No handler for: %[1]T=%+[1]v", req)
			}
		}
		log.Println("Disconnect client", conn.RemoteAddr())
		conn.Close()
	}
}

func handleHello(wr *vamp.WriteStream, req *protocol.HelloRequest) {
	log.Printf("hello at %s: req=%s / rsp=%s",
		req.MsgTime,
		hex.EncodeToString(req.ReqProto),
		hex.EncodeToString(req.RspProto),
	)
	ok := reflect.DeepEqual(req.ReqProto, protocol.ReqVersion)
	ok = ok && reflect.DeepEqual(req.RspProto, protocol.RspVersion)
	if !ok {
		log.Print("Protocol mismatch")
		must.Do(wr.WriteWithNext(protocol.RspProtoMismatch))
	}
	log.Print("Compatible protocol")
	must.Do(wr.WriteWithNext(protocol.RspOK))
}
