package wrind

import (
	"fmt"
	"os"
)

func Example() {
	iw := New(os.Stdout, "..")
	fmt.Fprintln(iw, "foo")
	iw.Indent(1)
	fmt.Fprintln(iw, "bar")
	iw.Indent(-1)
	fmt.Fprint(iw, "baz")
	// Output:
	// foo
	// ..bar
	// baz
}
