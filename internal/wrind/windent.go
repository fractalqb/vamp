package wrind

import (
	"bytes"
	"errors"
	"io"
)

type Writer struct {
	w          io.Writer
	indent     []byte
	linePrefix []byte
	startLine  bool
}

func New(w io.Writer, indent string) *Writer {
	return &Writer{w: w, indent: []byte(indent), startLine: true}
}

func (w *Writer) Indent(d int) error {
	switch {
	case d > 0:
		w.linePrefix = append(w.linePrefix, bytes.Repeat(w.indent, d)...)
	case d < 0:
		d = (-d) * len(w.indent)
		if d > len(w.linePrefix) {
			w.linePrefix = w.linePrefix[0:]
			return errors.New("unindent to negative level")
		}
		w.linePrefix = w.linePrefix[:len(w.linePrefix)-d]
	}
	return nil
}

func (w *Writer) Write(p []byte) (n int, err error) {
	for len(p) > 0 {
		m, err := w.prefix()
		n += m
		if err != nil {
			return n, err
		}
		i := bytes.IndexByte(p, '\n')
		if i < 0 {
			m, err = w.w.Write(p)
			w.startLine = false
			return n + m, err
		}
		i++
		m, err = w.w.Write(p[:i])
		n += m
		if err != nil {
			return n, err
		}
		p = p[i:]
		w.startLine = true
	}
	return n, nil
}

func (w *Writer) prefix() (int, error) {
	if w.startLine {
		n, err := w.w.Write(w.linePrefix)
		w.startLine = false // TODO check err 1st?
		return n, err
	}
	return 0, nil
}
