package internal

import (
	"io"

	"git.fractalqb.de/fractalqb/vamp/internal/wrind"
)

func WIndent(w io.Writer) (*wrind.Writer, bool) {
	if wi, ok := w.(*wrind.Writer); ok {
		return wi, false
	}
	return wrind.New(w, " "), true
}
