// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"errors"
	"fmt"
	"math"
	"reflect"
	"slices"
	"strings"
	"time"
)

// Fields denotes a set of fields from a Go struct. Fields are computed when a
// vamp record type is computed by reflection from a Go struct, e.g. with
// [Reflect.Type].
type Fields struct{ fs [][]int }

func (fs Fields) Empty() bool { return len(fs.fs) == 0 }

// Get copies the field values of from to an any slice and returs it. If reuse
// is not nil and has sufficient capacty it will be used to store the values.
// The result of Get is meant to be used with [Record.WriteFields].
func (fs Fields) Get(from any, reuse []any) []any {
	rv := reflect.Indirect(reflect.ValueOf(from))
	if rv.Kind() != reflect.Struct {
		return nil
	}
	if fsl := len(fs.fs); cap(reuse) < fsl {
		reuse = append(reuse, make([]any, fsl-len(reuse))...)
	} else {
		reuse = reuse[:fsl]
	}
	for i, idxs := range fs.fs {
		reuse[i] = rv.FieldByIndex(idxs).Interface()
	}
	return reuse
}

// Set writes pointers to the fields of to to an any slice and returns it. If
// reuse is not nil and has sufficient capacty it will be used to store the
// pointers. The result of Set is meant to be used with [Record.ReadFields].
func (fs Fields) Set(to any, reuse []any) []any {
	rv := reflect.Indirect(reflect.ValueOf(to))
	if rv.Kind() != reflect.Struct {
		return nil
	}
	if fsl := len(fs.fs); cap(reuse) < fsl {
		reuse = append(reuse, make([]any, fsl-len(reuse))...)
	} else {
		reuse = reuse[:fsl]
	}
	for i, idxs := range fs.fs {
		f := rv.FieldByIndex(idxs)
		if f.Kind() == reflect.Ptr {
			reuse[i] = f.Interface()
		} else {
			reuse[i] = f.Addr().Interface()
		}
	}
	return reuse
}

func ReflectType(val any) (Type, Fields, error) { return DefaultReflect().Type(val) }

func MustReflectType(val any) (Type, Fields) { return DefaultReflect().MustType(val) }

func UseStruct(s any) (*Record, error) { return DefaultReflect().UseStruct(s) }

func MustUseStruct(s any) *Record { return DefaultReflect().MustUseStruct(s) }

type Reflect struct {
	strSize   SizeRange
	arraySize SizeRange
}

func (r Reflect) StringSize(s SizeRange) Reflect {
	return Reflect{
		strSize:   s,
		arraySize: r.arraySize,
	}
}

func (r Reflect) ArraySize(s SizeRange) (res Reflect) {
	return Reflect{
		strSize:   r.strSize,
		arraySize: s,
	}
}

// DefaultReflect uses [Size16] for strings and [Size32] for arrays.
func DefaultReflect() Reflect {
	return Reflect{
		strSize:   Size16,
		arraySize: Size32,
	}
}

// UseStruct computes the vamp record type of a Go struct by reflection and
// calls [UseFieldsForStruct] on the computed fields for eficcient use without
// implementing [RecordMarshaler] and [RecordUnmarshaler].
func (r Reflect) UseStruct(s any) (*Record, error) {
	t, fs, err := r.Type(s)
	if err != nil {
		return nil, err
	}
	rec, ok := t.(*Record)
	if !ok {
		return nil, fmt.Errorf("%T is not refletced as record", s)
	}
	if err = UseFieldsForStruct(s, fs); err != nil {
		return nil, err
	}
	return rec, nil
}

func (r Reflect) MustUseStruct(s any) *Record {
	rec, err := r.UseStruct(s)
	if err != nil {
		panic(err)
	}
	return rec
}

func (r Reflect) Type(val any) (Type, Fields, error) {
	return r.reflectType(reflect.TypeOf(val), nil)
}

func (r Reflect) MustType(val any) (Type, Fields) {
	t, fs, err := r.Type(val)
	if err != nil {
		panic(err)
	}
	return t, fs
}

var (
	rTime     = reflect.TypeOf(time.Time{})
	rDuration = reflect.TypeOf(time.Duration(0))
)

func (r Reflect) reflectType(vt reflect.Type, srs []SizeRange) (Type, Fields, error) {
	switch {
	case vt == rTime:
		return Int64, Fields{}, nil
	case vt == rDuration:
		return Int64, Fields{}, nil
	}
	switch vt.Kind() {
	case reflect.Int:
		switch len(srs) {
		case 0:
			switch {
			case math.MaxInt < math.MaxInt16:
				return Int8, Fields{}, nil
			case math.MaxInt < math.MaxInt32:
				return Int16, Fields{}, nil
			case math.MaxInt < math.MaxInt64:
				return Int32, Fields{}, nil
			default:
				return Int64, Fields{}, nil
			}
		case 1:
			switch srs[0] {
			case Size8:
				return Int8, Fields{}, nil
			case Size16:
				return Int16, Fields{}, nil
			case Size32:
				return Int32, Fields{}, nil
			}
		default:
			return nil, Fields{}, errors.New("too many size-ranges from struct tag")
		}
	case reflect.Uint:
		switch len(srs) {
		case 0:
			switch {
			case math.MaxUint < math.MaxUint16:
				return Uint8, Fields{}, nil
			case math.MaxUint < math.MaxUint32:
				return Uint16, Fields{}, nil
			case math.MaxUint < math.MaxUint64:
				return Uint32, Fields{}, nil
			default:
				return Uint64, Fields{}, nil
			}
		case 1:
			switch srs[0] {
			case Size8:
				return Uint8, Fields{}, nil
			case Size16:
				return Uint16, Fields{}, nil
			case Size32:
				return Uint32, Fields{}, nil
			}
		default:
			return nil, Fields{}, errors.New("too many size-ranges from struct tag")
		}
	case reflect.Bool:
		return Bool, Fields{}, nil
	case reflect.Int8:
		return Int8, Fields{}, nil
	case reflect.Int16:
		return Int16, Fields{}, nil
	case reflect.Int32:
		return Int32, Fields{}, nil
	case reflect.Int64:
		return Int64, Fields{}, nil
	case reflect.Uint8:
		return Uint8, Fields{}, nil
	case reflect.Uint16:
		return Uint16, Fields{}, nil
	case reflect.Uint32:
		return Uint32, Fields{}, nil
	case reflect.Uint64:
		return Uint64, Fields{}, nil
	case reflect.Float32:
		return Float32, Fields{}, nil
	case reflect.Float64:
		return Float64, Fields{}, nil
	case reflect.String:
		switch len(srs) {
		case 0:
			st := NewBytes(r.strSize, ReadString)
			return st, Fields{}, nil
		case 1:
			st := NewBytes(srs[0], ReadString)
			return st, Fields{}, nil
		default:
			return nil, Fields{}, errors.New("too many size-ranges from struct tag")
		}
	case reflect.Slice:
		asz := r.arraySize
		if len(srs) > 0 {
			asz = srs[0]
			srs = srs[1:]
		}
		if vt.Elem().Kind() == reflect.Uint8 {
			return NewBytes(asz, ReadRaw), Fields{}, nil
		}
		et, _, err := r.reflectType(vt.Elem(), srs)
		if err != nil {
			return nil, Fields{}, err
		}
		return NewArray(asz, et), Fields{}, nil
	case reflect.Struct:
		var refl structRefl
		err := r.reflectStruct(vt, &refl)
		if err != nil {
			return nil, Fields{}, err
		}
		rt := NewRecord(refl.typs...)
		rt.names = refl.names
		return rt, Fields{refl.fields}, nil
	case reflect.Map:
		msz := r.arraySize
		if len(srs) > 0 {
			msz = srs[0]
			srs = srs[1:]
		}
		key, _, err := r.reflectType(vt.Key(), srs) // TODO srs handling
		if err != nil {
			return nil, Fields{}, err
		}
		val, _, err := r.reflectType(vt.Elem(), srs) // TODO srs handling
		if err != nil {
			return nil, Fields{}, err
		}
		et := NewRecord(MustNamed("key", key), MustNamed("val", val))
		return NewArray(msz, et), Fields{}, nil
	case reflect.Array:
		var asz SizeRange
		if len(srs) > 0 {
			asz = srs[0]
			srs = srs[1:]
		} else {
			l := vt.Size() / vt.Elem().Size()
			var err error
			if asz, err = SizeRangeFromMax(uint(l)); err != nil {
				return nil, Fields{}, err
			}
		}
		if vt.Elem().Kind() == reflect.Uint8 {
			return NewBytes(asz, ReadRaw), Fields{}, nil
		}
		et, _, err := r.reflectType(vt.Elem(), srs)
		if err != nil {
			return nil, Fields{}, err
		}
		return NewArray(asz, et), Fields{}, nil
	}
	return nil, Fields{}, fmt.Errorf("no vamp type for %s", vt.String())
}

type structRefl struct {
	typs   []Type
	names  []string
	fields [][]int
	idx    []int
}

func (r Reflect) reflectStruct(vt reflect.Type, refl *structRefl) error {
	depth := len(refl.idx)
	refl.idx = append(refl.idx, 0)
	defer func() { refl.idx = refl.idx[:depth] }()
	for i := 0; i < vt.NumField(); i++ {
		refl.idx[depth] = i
		f := vt.Field(i)
		switch {
		case f.Anonymous && f.Type.Kind() == reflect.Struct:
			if err := r.reflectStruct(f.Type, refl); err != nil {
				return err
			}
		case f.IsExported():
			tag := fieldTag{use: true, name: f.Name}
			if t := f.Tag.Get("vamp"); t != "" {
				var err error
				tag, err = parseTag(tag.name, t)
				if err != nil {
					return err
				}
				if !tag.use {
					continue
				}
			}
			e, _, err := r.reflectType(f.Type, tag.size)
			if err != nil {
				return fmt.Errorf("vamp reflect field %d/%d '%s' in %s: %w",
					i, vt.NumField(),
					f.Name,
					vt,
					err,
				)
			}
			refl.typs = append(refl.typs, e)
			refl.names = append(refl.names, tag.name)
			refl.fields = append(refl.fields, slices.Clone(refl.idx))
		}
	}
	return nil
}

type fieldTag struct {
	use  bool
	name string
	size []SizeRange
}

func parseTag(name string, tag string) (fieldTag, error) {
	args := strings.Split(tag, ",")
	if len(args) == 0 {
		return fieldTag{true, name, nil}, nil
	}
	if n := strings.TrimSpace(args[0]); n == "-" {
		return fieldTag{use: false}, nil
	} else if n != "" {
		name = n
	}
	if len(args) > 2 {
		return fieldTag{}, fmt.Errorf("vamp struct tag on '%s' with more than 2 arguments", name)
	}
	var szRanges []SizeRange
	for _, arg := range args[1:] {
		switch {
		case strings.HasPrefix(arg, "size="):
			var err error
			if szRanges, err = parseSizeRanges(arg[5:]); err != nil {
				return fieldTag{}, err
			}
		default:
			return fieldTag{}, fmt.Errorf("vamp tag syntax at '%s'", arg)
		}
	}
	return fieldTag{true, name, szRanges}, nil
}

func parseSizeRanges(s string) ([]SizeRange, error) {
	rs := strings.Split(s, ":")
	if len(rs) == 0 {
		return nil, fmt.Errorf("vamp struct tag: no sizes")
	}
	res := make([]SizeRange, len(rs))
	for i, r := range rs {
		switch r {
		case "8":
			res[i] = Size8
		case "16":
			res[i] = Size16
		case "32":
			res[i] = Size32
		default:
			return nil, fmt.Errorf("invalid size range tag '%s'", r)
		}
	}
	return res, nil
}
