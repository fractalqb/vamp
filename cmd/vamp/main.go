// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

/*
The vamp CLI tool converts type definitions between vamp and text encoding and
outputs vamp data as JSON.
*/
package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"path/filepath"

	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/vamp"
)

var cfg = struct {
	typeInfo string
	indent   string
	output   string
	offSize  int
	noTPrint bool
}{
	typeInfo: "any",
	offSize:  32,
}

func main() {
	flag.StringVar(&cfg.typeInfo, "t", cfg.typeInfo,
		"Set type",
	)
	flag.StringVar(&cfg.indent, "i", cfg.indent,
		"Set JSON indent",
	)
	flag.StringVar(&cfg.output, "o", cfg.output,
		"Set output file",
	)
	flag.IntVar(&cfg.offSize, "offset", cfg.offSize,
		"Set the number of bits for writing Vampire offests",
	)
	flag.BoolVar(&cfg.noTPrint, "p", cfg.noTPrint,
		"Don't pretty-print types",
	)
	flag.Parse()

	var typ vamp.Type
	switch filepath.Ext(cfg.typeInfo) {
	case ".vmpt":
		data := must.Ret(os.ReadFile(cfg.typeInfo))
		buf := must.Ret(vamp.NewTypeReadBuffer(data, false))
		typ = must.Ret(vamp.ReadType(buf))
	case ".vmps":
		data := must.Ret(os.ReadFile(cfg.typeInfo))
		typ = must.Ret(vamp.ParseType(bytes.NewReader(data)))
	default:
		typ = must.Ret(vamp.ParseTypeString(cfg.typeInfo))
	}

	if flag.NArg() == 0 {
		switch {
		case cfg.output == "":
			if cfg.noTPrint {
				fmt.Println(typ.String())
			} else {
				typ.PrintTo(os.Stdout)
			}
		case filepath.Ext(cfg.output) == ".vmpt":
			szRange := must.Ret(vamp.SizeRangeFromBits(cfg.offSize))
			buf := vamp.NewTypeWriteBuffer(
				szRange,
				vamp.HeaderSize,
				vamp.HeaderFor(szRange).Bytes(0),
			)
			must.Do(vamp.WriteType(buf, typ))
			must.Do(os.WriteFile(cfg.output, buf.AllBytes(), 0666))
		default:
			w := must.Ret(os.Create(cfg.output))
			defer w.Close()
			if cfg.noTPrint {
				fmt.Fprintln(w, typ.String())
			} else {
				typ.PrintTo(w)
			}
		}
		return
	}

	var enc *json.Encoder
	if cfg.output == "" {
		enc = json.NewEncoder(os.Stdout)
	} else {
		w := must.Ret(os.Create(cfg.output))
		enc = json.NewEncoder(w)
	}
	if cfg.indent != "" {
		enc.SetIndent("", cfg.indent)
	}

	for _, arg := range flag.Args() {
		data := must.Ret(os.ReadFile(arg))
		buf := must.Ret(vamp.NewReadBufferHeader(typ, data, false))
		var v any
		must.Do(typ.Read(buf, &v))
		must.Do(enc.Encode(v))
	}
}
