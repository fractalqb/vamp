// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"fmt"
	"maps"
	"math"
	"reflect"
	"testing"
	"time"

	"git.fractalqb.de/fractalqb/testerr"
)

func ExampleTypeOf() {
	type animal struct {
		Age    int      `vamp:"age,size=8"`
		Name   string   `vamp:",size=8"`
		Owners []string `vamp:",size=8:8"`
		Male   bool     `vamp:"-"`
	}
	t, _, err := ReflectType(animal{})
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(t.String())
	}
	// Output:
	// {age:int8 Name:"Size8" Owners:[Size8 "Size8"]}
}

func TestReflectType_byteArray(t *testing.T) {
	typ, _, err := ReflectType([]byte{})
	testerr.Shall(err).BeNil(t)
	if !Equal(typ, NewBytes(DefaultReflect().arraySize, ReadRaw), 0) {
		t.Error(typ)
	}
}

func TestReflectType_time(t *testing.T) {
	typ, _, err := ReflectType(time.Time{})
	testerr.Shall(err).BeNil(t)
	if typ != Int64 {
		t.Errorf("Unexpected reflected type %s", typ)
	}
	buf := NewWriteBuffer(typ, Size8, 0, nil)
	now := time.Now()
	testerr.Shall(typ.Write(buf, now)).BeNil(t)
	var out time.Time
	testerr.Shall(typ.Read(&buf.ReadBuffer, &out)).BeNil(t)
	if out.UnixMilli() != now.UnixMilli() {
		t.Errorf("out %s != %s", out, now)
	}
}

func TestReflectType_duration(t *testing.T) {
	typ, _, err := ReflectType(time.Second)
	testerr.Shall(err).BeNil(t)
	if typ != Int64 {
		t.Errorf("Unexpected reflected type %s", typ)
	}
	buf := NewWriteBuffer(typ, Size8, 0, nil)
	d := 35 * time.Hour
	testerr.Shall(typ.Write(buf, d)).BeNil(t)
	var out time.Duration
	testerr.Shall(typ.Read(&buf.ReadBuffer, &out)).BeNil(t)
	if d != out {
		t.Errorf("out %s != %s", out, d)
	}
}

func TestReflectType_fields(t *testing.T) {
	type emb struct {
		Age  int
		Name string
	}
	type animal struct {
		emb
		Owners []string `vamp:"-"`
		Male   bool
	}
	_, fs, err := ReflectType(animal{})
	testerr.Shall(err).BeNil(t)

	t.Run("get", func(t *testing.T) {
		a := animal{emb{4, "Cat"}, nil, false}
		fvals := fs.Get(a, nil)
		if v := fvals[0]; v != a.Age {
			t.Error("Age:", v)
		}
		if v := fvals[1]; v != a.Name {
			t.Error("Name:", v)
		}
		if v := fvals[2]; v != a.Male {
			t.Error("Male:", v)
		}
	})

	t.Run("set", func(t *testing.T) {
		var a animal
		fptrs := fs.Set(&a, nil)
		*(fptrs[0].(*int)) = 4
		*(fptrs[1].(*string)) = "Cat"
		*(fptrs[2].(*bool)) = true
		if !reflect.DeepEqual(a, animal{emb{4, "Cat"}, nil, true}) {
			t.Errorf("%+v", a)
		}
	})
}

func TestReflectType_map(t *testing.T) {
	src := map[string]float64{
		"pi": math.Pi,
		"e":  math.E,
	}
	vt, _, err := ReflectType(src)
	testerr.Shall(err).BeNil(t)
	const offSz = Size16
	wbuf := NewWriteBuffer(vt, offSz, 0, nil)
	testerr.Shall(vt.Write(wbuf, src)).BeNil(t)
	rbuf := NewReadBuffer(vt, offSz, wbuf.AllBytes())
	var dst map[string]float64
	testerr.Shall(vt.Read(rbuf, &dst)).BeNil(t)
	if !maps.Equal(dst, src) {
		t.Error("mismatch:", dst, "<-", src)
	}
}
