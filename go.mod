module git.fractalqb.de/fractalqb/vamp

go 1.22.0

toolchain go1.23.1

require (
	git.fractalqb.de/fractalqb/eloc v0.3.0
	git.fractalqb.de/fractalqb/testerr v0.1.1
	golang.org/x/exp v0.0.0-20240909161429-701f63a606c0
)
