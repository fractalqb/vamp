![Vampire](./Vampire.png)
# Binary Data Serialization

[![Go Reference](https://pkg.go.dev/badge/git.fractalqb.de/fractalqb/vamp.svg)](https://pkg.go.dev/git.fractalqb.de/fractalqb/vamp)

Although it is not zero-copy, Vampire provides "direct access" to values, which
means you can navigate down any path in a nested data structure without having
to analyze too many other parts that come before the value you are looking for.

This ability to reach each data field quickly makes the format accessible –
“perviam” in Latin. Just because “Vampire” sounds much cooler the format is
called Vampire in the end. But `vamp` is the package name to Go for because its
shorter and still associated with vampires.

_What is the point of this property for a serialization format?_

Message brokers, queuing systems, and other types of middleware often have to
make decisions about the data they manage based on tiny pieces of that data.
This is often solved by artificially and redundantly adding the desired
information as out-of-band data to the normal payload data.  Alternatively, one
could decode the data first and then find the relevant parts within the payload.
If you choose the second approach, it is more efficient to decode only the
relevant parts of the payload.

That was the driving idea for the design of Vampire.

## Data Types

To make Vampire work, it requires strict typing of formats. Types must be known
before writing or reading data. This reduces the amount of structural
information in the encoded data and makes Vampire data quite compact. Vampire
types have a Vampire encoding that allows us to create self-contained and
self-descriptive data. For convenience, types also have a concise text encoding,
which is described here.

``` EBNF
TYPE ::= BASIC_TYPE | BYTES_TYPE | ARRAY_TYPE | RECORD_TYPE | LIST_TYPE |
         UNION_TYPE | ANY_TYPE
```

``` EBNF
SIZE ::= 'Size8' | 'Size16' | 'Size32'
```


* **Basic Types:** `bool`; signed and unsigned integers with 8, 16, 32 and 64
  bit; 32-bit and 64-bit float

  ``` EBNF
  BASIC_TYPE ::= 'bool' | 'int8' | 'int16' | 'int32' | 'int64' | 'uint8' |
                 'uint16' | 'uint32' | 'unit64' | 'float32' | 'float64'
  ```

* **Bytes Type:** Byte strings with either 8-, 16- or 32-bit size limits (size
  in Byte)

  ``` EBNF
  BYTES_TYPE ::= '"' SIZE '"'`
  ```

* **Array Types:** Variable length array with 8-bit, 16-bit and 32-bit length
  limits. Each element has the same type. Length is part of the array value not
  the array type. The length of an array must be known before writing an array
  value.

  ``` EBNF
  ARRAY_TYPE ::= '[' SIZE ' ' TYPE ']'
  ```

* **Record Types:** Fixed number of fields where each field has it's own type.
  Field names are optional and are not part of record values.

  ``` EBNF
  RECORD_TYPE ::= '{' [ FIELD { ' ' FIELD } ] '}'
  ```

  ``` EBNF
  FIELD ::= [ FIELD_NAME ':' ] TYPE
  ```

* **List Types:** Unbounded (theoretically) sequence of values of the same type.
  Final length does not need to be known before writing. A 8-, 16- or 32-bit
  length fields is provided. If the final length fits, it might be recorded in
  the list value.

  ``` EBNF
  LIST_TYPE ::= '(' SIZE ' ' TYPE ')'
  ```

* **Union Types:** A tagged union with a predefined set of alternative
  value types. Tags can have 8-, 16- or 32-bit size.

  ``` EBNF
  UNION_TYPE ::= '<' SIZE ' ' TYPE { '|' TYPE } '>'
  ```

* **Any Type:** Write any Vampire value along with its Vampire type.

  ``` EBNF
  ANY_TYPE ::= 'any'
  ```

## Performance

### Full Record Write and Read

Using this Go data structure with Vampire type `{Age:int8 Name:"Size8" Owners:[Size8 "Size8"] Male:bool}`

``` Go
type Animal struct {
	Age    int      `vamp:",size=8"`
	Name   string   `vamp:",size=8"`
	Owners []string `vamp:",size=8:8"`
	Male   bool
}

Animal{Age: 4, Name: "Candy", Owners: []string{"Mary", "Joe"}, Male:   true}
```

| Format         | Encoded Bytes | Write&Read-Time / Best |
|----------------|--------------:|-------------:|
| Vampire (8-bit offsets)  |  22 |            – |
| Vampire (16-bit offsets) |  26 |         100% |
| Vampire (32-bit offsets) |  34 |            – |
| JSON ([Go std. lib](https://pkg.go.dev/encoding/json)) |  60 | 233% |
| JSON ([jsoniter](https://github.com/json-iterator/go)) |  60 | 110% |
| [GOB](https://pkg.go.dev/encoding/gob)    | 113 | 2,130% |
| [CBOR](https://github.com/fxamacker/cbor) |  40 |  112% |

### Partial Read Performance

This is a test from the
[vamp-benchmarks](https://codeberg.org/fractalqb/vamp-benchmarks) project. It runs on 100,000 randomly generated orders with the structure:

``` Go
type Order struct {
	CustName string
	CustNum  uint32
	Date     time.Time
	Lines    []OrderLine
}

type OrderLine struct {
	MatName string
	MatNum  uint32
	Count   int
	Price   float64
}
```

The task is to filter all orders that have at least one order line with the
MatNum 0. The test targets Vampire's core property of being able to access
individual attributes as directly as possible.

| Format    | File Size        | real    | user    | sys     | total   | orders/s |
|-----------|-----------------:|--------:|--------:|--------:|--------:|---------:|
| JSON      | 124,023,274 byte | 1.447 s | 1.022 s | 0.401 s | 2.869 s |   34,855 |
| vamp      |  52,374,536 byte | 0.220 s | 0.056 s | 0.163 s | 0.438 s |  228,311 |
| vamp/JSON |           42.23% |  15.19% |   5.43% |  40.70% |  15.28% |     6.55 |

_JSON timing was measured with [Jsoniter](https://github.com/json-iterator/go).
File sizes may vary with new test runs becaus of randomly generated data._
