// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"fmt"
	"reflect"
	"testing"
)

func ExampleArray() {
	a := []string{"boo", "bar", "baz"}
	t, _, _ := ReflectType(a)
	fmt.Println(t)
	buf := NewWriteBuffer(t, Size16, 0, nil)
	t.Write(buf, a)
	var out []string
	t.Read(&buf.ReadBuffer, &out)
	fmt.Printf("%[1]T=%[1]v\n", out)
	// Output:
	// [Size32 "Size16"]
	// []string=[boo bar baz]
}

func TestArray(t *testing.T) {
	ety := NewBytes(Size16, ReadRaw)
	aty := NewArray(Size16, ety)
	buf := NewWriteBuffer(aty, Size8, 0, nil)
	t.Run("write & read slice", func(t *testing.T) {
		var data = []any{"foo", "bar", "baz"}
		buf.Reset(aty, Size16, 0)
		err := aty.Write(buf, data)
		if err != nil {
			t.Fatal(err)
		}
		var out []string
		err = aty.Read(&buf.ReadBuffer, &out)
		if err != nil {
			t.Fatal(err)
		}
		if len(out) != 3 {
			t.Fatalf("wrong length %d of output", len(out))
		}
		for i := 0; i < 3; i++ {
			if data[i] != out[i] {
				t.Errorf("wrong value '%s' in element %d='%s'", out[i], i, data[i])
			}
		}
	})
	t.Run("write & read array", func(t *testing.T) {
		buf.Reset(aty, Size16, 0)
		err := aty.Write(buf, [3]string{"foo", "bar", "baz"})
		if err != nil {
			t.Fatal(err)
		}
		var out [3]string
		err = aty.Read(&buf.ReadBuffer, &out)
		if err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(out, [3]string{"foo", "bar", "baz"}) {
			t.Errorf("wrong output: %v", out)
		}
	})
}

func TestArray_randomRead(t *testing.T) {
	ety := NewBytes(Size8, ReadRaw)
	aty := NewArray(Size32, ety)
	buf := NewWriteBuffer(aty, Size32, 0, nil)
	strs := []string{"foo", "bar", "baz", "quux"}
	err := aty.Write(buf, strs)
	if err != nil {
		t.Fatal(err)
	}
	for i := len(strs) - 1; i >= 0; i-- {
		rev := aty.Index(&buf.ReadBuffer, uint(i))
		s := ety.ReadString(&buf.ReadBuffer)
		buf.Unslice(rev)
		if s != strs[i] {
			t.Errorf("wrong string '%s'", s)
		}
	}
}

func TestArray_Write_tooLong(t *testing.T) {
	data := make([]int8, 256)
	typ := NewArray(Size8, Int8)
	buf := NewWriteBuffer(typ, Size8, 0, nil)
	err := typ.Write(buf, data)
	if err == nil {
		t.Fatal("no error")
	}
	if err.Error() != "writing array: size 256 out of uint8 range" {
		t.Errorf("wrong error: '%s'", err)
	}
}

func ExampleNewOptional() {
	et := NewBytes(Size8, ReadRaw)
	ot := NewOptional(et)
	buf := NewWriteBuffer(ot, Size8, 0, nil)
	ot.WriteOptOne(buf, "Hello, optional!")
	var out string
	ok, _ := ot.ReadOpt(&buf.ReadBuffer, &out)
	fmt.Println(ok, out)
	buf.Reset(ot, Size16, 0)
	ot.WriteOptNone(buf)
	out = "-"
	ok, _ = ot.ReadOpt(&buf.ReadBuffer, &out)
	fmt.Println(ok, out)
	// Output:
	// true Hello, optional!
	// false -
}
