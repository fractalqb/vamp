// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"fmt"
	"hash"
	"io"
	"os"
)

var IOBytesMode = CheckUTF8

func WriteTypeFile(name string, off SizeRange, t Type) error {
	buf := NewTypeWriteBuffer(off, HeaderSize, HeaderFor(off).Bytes(0))
	if err := WriteType(buf, t); err != nil {
		return err
	}
	return os.WriteFile(name, buf.AllBytes(), 0666)
}

func ReadTypeFile(name string) (Type, error) {
	data, err := os.ReadFile(name)
	if err != nil {
		return nil, err
	}
	buf, err := NewTypeReadBuffer(data, false)
	return ReadType(buf)
}

func NewTypeWriteBuffer(off SizeRange, at int, buf []byte) *Buffer {
	return NewWriteBuffer(&typeIO, off, at, buf)
}

func NewTypeReadBuffer(buf []byte, search bool) (*ReadBuffer, error) {
	return NewReadBufferHeader(&typeIO, buf, search)
}

func NewTypeReadBufferOffSize(off SizeRange, buf []byte) *ReadBuffer {
	return NewReadBuffer(&typeIO, off, buf)
}

func ResetTypeBuffer(buf *Buffer, off SizeRange, prefix int) {
	buf.Reset(&typeIO, off, prefix)
}

func WriteType(buf *Buffer, t Type) error {
	return typeIO.Write(buf, t)
}

func ReadType(buf *ReadBuffer) (t Type, err error) {
	_, err = typeIO.ReadAlt(buf, func(alt uint) (any, error) {
		return altTypes(alt, &t)
	})
	if err != nil {
		return nil, err
	}
	return t, nil
}

const (
	boolAlt = iota
	int8Alt
	int16Alt
	int32Alt
	int64Alt
	uint8Alt
	uint16Alt
	uint32Alt
	uint64Alt
	float32Alt
	float64Alt
	bytesAlt
	arrayAlt
	recordAlt
	listAlt
	unionAlt
	anyAlt // TODO implement typeIO
)

func init() {
	typeIO.alts = []Type{
		btIO[boolType]{},
		btIO[int8Type]{},
		btIO[int16Type]{},
		btIO[int32Type]{},
		btIO[int64Type]{},
		btIO[uint8Type]{},
		btIO[uint16Type]{},
		btIO[uint32Type]{},
		btIO[uint64Type]{},
		btIO[float32Type]{},
		btIO[float64Type]{},
		Uint8,    // string
		arrayIO,  // array
		recordIO, // record
		listIO,   // list
		unionIO,  // union
		btIO[anyType]{},
	}
	typeIO.AltTarget = altTypes
}

type btIO[T Type] struct{}

func (btIO[T]) FixSize() uint               { return 0 }
func (btIO[T]) DynNo() uint                 { return 0 }
func (btIO[T]) Write(*Buffer, any) error    { return nil }
func (btIO[T]) Read(*ReadBuffer, any) error { return nil }

func (btIO[T]) Compare(t Type, _ CompareOpts) (bool, int) {
	return sameAs[btIO[T]](t)
}

func (t btIO[T]) Hash(h hash.Hash, _ CompareOpts) {
	io.WriteString(h, t.String())
}

func (btIO[T]) String() string {
	var tmp T
	return fmt.Sprintf("IO:%s", tmp)
}

func (t btIO[T]) PrintTo(w io.Writer) (int, error) {
	return io.WriteString(w, t.String())
}

var typeIO = Union{
	szRange: Size8,
}

var arrayIO = NewRecord(
	Uint8,
	&typeIO,
)

var recordIO = NewArray(
	Size16, // Records have max 2^16-1 fields
	NewRecord(
		NewBytes(Size8, ReadString),
		&typeIO,
	),
)

var listIO = NewRecord(
	Uint8,
	&typeIO,
)

// TODO IO for union's alt names as well
var unionIO = NewRecord(
	Uint8,
	NewArray(
		Size16, // Unions have max 2^16-1 variants
		NewRecord(
			NewBytes(Size8, ReadString),
			&typeIO,
		),
	),
)

func altTypes(alt uint, into any) (any, error) {
	in, ok := into.(*Type)
	if !ok {
		return nil, fmt.Errorf("cannot read type %d into %T", alt, into)
	}
	switch alt {
	case boolAlt:
		*in = Bool
		return Bool, nil
	case int8Alt:
		*in = Int8
		return Int8, nil
	case int16Alt:
		*in = Int16
		return Int16, nil
	case int32Alt:
		*in = Int32
		return Int32, nil
	case int64Alt:
		*in = Int64
		return Int64, nil
	case uint8Alt:
		*in = Uint8
		return Uint8, nil
	case uint16Alt:
		*in = Uint16
		return Uint16, nil
	case uint32Alt:
		*in = Uint32
		return Uint32, nil
	case uint64Alt:
		*in = Uint64
		return Uint64, nil
	case float32Alt:
		*in = Float32
		return Float32, nil
	case float64Alt:
		*in = Float64
		return Float64, nil
	case bytesAlt:
		t := &Bytes{mode: IOBytesMode}
		*in = t
		return t, nil
	case arrayAlt:
		t := new(Array)
		*in = t
		return t, nil
	case recordAlt:
		t := new(Record)
		*in = t
		return t, nil
	case listAlt:
		t := new(List)
		*in = t
		return t, nil
	case unionAlt:
		t := new(Union)
		*in = t
		return t, nil
	case anyAlt:
		*in = Any
		return Any, nil
	}
	return nil, fmt.Errorf("invalid type alt: %d", alt)
}
