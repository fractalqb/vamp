// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"reflect"
	"testing"
)

func TestVersion(t *testing.T) {
	if Version >= 0xFC {
		t.Fatalf("Version range 0–%d exceeded: %d", 0xfc-1, Version)
	}
}

func ExampleHeader() {
	fmt.Println(HeaderFor(Size16))
	// Output:
	// Vampire-v0+16bit
}

func TestBytesType_bytes(t *testing.T) {
	strType := NewBytes(Size16, ReadRaw)
	buf := NewWriteBuffer(strType, Size8, 0, nil)
	err := strType.WriteBytes(buf, []byte("Hello, vampire!"))
	if err != nil {
		t.Fatal(err)
	}
	t.Run("read bytes", func(t *testing.T) {
		bs := strType.ReadBytes(&buf.ReadBuffer)
		if string(bs) != "Hello, vampire!" {
			t.Errorf("Unexpected string %s", bs)
		}
	})
	t.Run("read into bytes", func(t *testing.T) {
		var bs []byte
		err = strType.Read(&buf.ReadBuffer, &bs)
		if err != nil {
			t.Fatal(err)
		}
		if string(bs) != "Hello, vampire!" {
			t.Errorf("Unexpected string %v", bs)
		}
	})
	t.Run("read into any", func(t *testing.T) {
		var bs any
		err = strType.Read(&buf.ReadBuffer, &bs)
		if err != nil {
			t.Fatal(err)
		}
		if string(bs.([]byte)) != "Hello, vampire!" {
			t.Errorf("Unexpected string '%s'", bs)
		}
	})
}

func TestBytesType_string(t *testing.T) {
	strType := NewString(Size16)
	buf := NewWriteBuffer(strType, Size8, 0, nil)
	err := strType.WriteString(buf, "Hello, vampire!")
	if err != nil {
		t.Fatal(err)
	}
	t.Run("read string", func(t *testing.T) {
		s := strType.ReadString(&buf.ReadBuffer)
		if s != "Hello, vampire!" {
			t.Errorf("Unexpected string '%s'", s)
		}
	})
	t.Run("read into string", func(t *testing.T) {
		var s string
		err = strType.Read(&buf.ReadBuffer, &s)
		if err != nil {
			t.Fatal(err)
		}
		if s != "Hello, vampire!" {
			t.Errorf("Unexpected string '%s'", s)
		}
	})
	t.Run("read into any", func(t *testing.T) {
		var s any
		err = strType.Read(&buf.ReadBuffer, &s)
		if err != nil {
			t.Fatal(err)
		}
		if s.(string) != "Hello, vampire!" {
			t.Errorf("Unexpected string '%s'", s)
		}
	})
}

func TestBytesType_Compare(t *testing.T) {
	s1 := NewBytes(Size16, ReadRaw)
	s2 := NewBytes(Size16, ReadRaw)
	s3 := NewBytes(Size8, ReadRaw)
	if !Equal(s1, s2, 0) {
		t.Error("s1 != s2")
	}
	if !Equal(&s1, s2, 0) {
		t.Error("&s1 != s2")
	}
	if !Equal(s1, &s2, 0) {
		t.Error("s1 != &s2")
	}
	if !Equal(&s1, &s2, 0) {
		t.Error("&s1 != &s2")
	}
	if Equal(s1, s3, 0) {
		t.Errorf("s1 == s3")
	}
}

func BenchmarkStringWrite(b *testing.B) {
	strType := NewBytes(Size16, ReadRaw)
	buf := NewWriteBuffer(strType, Size16, 0, make([]byte, 64))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf.Reset(strType, Size16, 0)
		err := strType.WriteString(buf, "Hello, vampire!")
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkStringBin(b *testing.B) {
	const msg = "Hello, vampire!"
	var buf bytes.Buffer
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buf.Reset()
		err := binary.Write(&buf, binary.LittleEndian, uint16(len(msg)))
		if err != nil {
			b.Fatal(err)
		}
		_, err = buf.WriteString(msg)
		if err != nil {
			b.Fatal(err)
		}
	}
}

func FuzzBytesType_bytes(f *testing.F) {
	testcases := [][]byte{
		{},
		[]byte("0"),
		{0, 2, 3},
	}
	for _, tc := range testcases {
		f.Add(tc)
	}
	typ := NewBytes(Size16, ReadRaw)
	buf := NewWriteBuffer(typ, Size8, 0, nil)
	f.Fuzz(func(t *testing.T, a []byte) {
		buf.Reset(typ, Size8, 0)
		if err := typ.Write(buf, a); err != nil {
			t.Fatal(err)
		}
		var out []byte
		if err := typ.Read(&buf.ReadBuffer, &out); err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(out, a) {
			t.Errorf("out %s != %s", out, a)
		}
	})
}

func FuzzBytesType_string(f *testing.F) {
	testcases := []string{"", "0", "foo", "a somewhat longer string\nwith a linebreak"}
	for _, tc := range testcases {
		f.Add(tc)
	}
	typ := NewBytes(Size16, ReadRaw)
	buf := NewWriteBuffer(typ, Size8, 0, nil)
	f.Fuzz(func(t *testing.T, a string) {
		buf.Reset(typ, Size8, 0)
		if err := typ.Write(buf, a); err != nil {
			t.Fatal(err)
		}
		var out string
		if err := typ.Read(&buf.ReadBuffer, &out); err != nil {
			t.Fatal(err)
		}
		if out != a {
			t.Errorf("out %s != %s", out, a)
		}
	})
}
