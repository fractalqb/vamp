// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

package vamp

import "fmt"

func ExampleAny() {
	a := []string{"boo", "bar", "baz"}
	t, _, _ := ReflectType(a)
	buf := NewWriteBuffer(Any, Size16, 0, nil)
	Any.WriteAny(buf, t, a)
	outType, out, _ := Any.ReadAny(&buf.ReadBuffer)
	fmt.Println(outType)
	fmt.Printf("%[1]T=%[1]v\n", out)
	// Output:
	// [Size32 "Size16"]
	// []interface {}=[boo bar baz]
}
