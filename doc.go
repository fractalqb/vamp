// Copyright 2022 Marcus Perlick
// This file is part of Go module git.fractalqb.de/fractalqb/vamp
//
// vamp is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// vamp is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with change.  If not, see <http://www.gnu.org/licenses/>.

/*
Package vamp implements the Vampire binary data serialization format for Go.

While Vampire does not provide zero-copy characteristics it gives you "direct
access" to values i.e., you can navigate any path down into nested structures
without parsing too many other parts that come before the value you are looking
for. This feature makes any value directly accessible – “perviam” in Latin. Just
because “Vampire” sounds much cooler the format is called Vampire in the end.
But vamp is the package name to Go for because its shorter and still associated
with vampires.

# Type Promotion

When reading and writing data, Vamp supports type promotion between certain
types to facilitate the preservation of backwards compatibility.

  - Type promotion between integers is supported if a source type can be stored
    in a target type without loss. E.g. uint16 can be promoted to int32 but no
    signed integer type can be promoted to any unsigned integer type.

  - float32 will be promoted to `float64`

For the machine-dependent integers int and uint, it depends on the current value
whether it can be read from or written to a Vampire integer. If the current
value is within the range of the target, the read or write is successful.
Otherwise, it results in a runtime error.

# Date and Duration

By default as int64 <-> Unix time with millis.
*/
package vamp
